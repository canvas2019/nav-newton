#pragma once
#include<common.h>
#include<vector>
#include<iostream>
#include "config.h"
#include <../Eigen/Dense>
#include <../Eigen/Core>
using namespace Eigen;
namespace GNSS_parameter//GNSS构造参数
{
	const int GNSS_data_size = 0;
	const double GNSS_begin_time = 0.0;
	const double GNSS_end_time = 0.0;
	const double GNSS_sampling_interval = 1.0;//采样间隔
}
using namespace GNSS_parameter;
using namespace std;
/*每个历元的卫星结果信息*/
struct GNSSEpoch {
	int epoch_index;//卫星结果历元序号
	double tow;//GPS周秒
	double _sampling_interval;//与上一历元的GNSS观测时的时间间隔（首个历元设1）
	Vector3d _gnss_pos;//纬度，经度，(rad)高程
	Vector3d _gnss_vel;//北东地速度
	Matrix3d _Dxx;//经纬高结果方差阵
	Matrix3d _Dvv;//北东地结果方差阵
};

class GNSS
{
public:
	////构造函数
	//GNSS(int _data_size = IMU_data_size, double _begin_time = IMU_begin_time, double _end_time = IMU_end_time, double _sampling_interval = GNSS_sampling_interval)
	//{
	//	data_size = _data_size;
	//	begin_time = _begin_time;
	//	end_time = _end_time;
	//	sampling_interval = _sampling_interval;
	//}
public://这里放接口函数
	void readFile(string name);
public:
	//获取GNSS-RTK解算结果
	void getGNSSres(int epoch_index, GNSSEpoch &gnss_epoch);
public:
	int _data_size;//GNSS数据长度
	double _begin_time;//GNSS数据开始时间(GPS周秒)
	double _end_time;//GNSS数据结束时间(GPS周秒)
	//vector<double>base_position_XYZ;//基站位置XYZ(m,m,m)
	//vector<double>base_position_BLH;//基站位置NEU(deg,deg,m)
public:
	double sampling_interval;//采样间隔
	vector<vector<double>>_GNSS_result;//所有卫星结果的vector
};

