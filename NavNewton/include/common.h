/*---------------------------------------------
   navicommon.h
   create on 12 Apr 2021 ZHUOXU WHU
---------------------------------------------*/

#ifndef _COMMON_H_
#define _COMMON_H_

#include <math.h>
#include <iostream>
#include <string>
#include <vector>
#include "../Eigen/Dense"

using namespace std;
using namespace Eigen;
// satellite related defination ------------------------------------
#define SYS_NONE    0x00    /* navigation system: NONE */
#define SYS_GPS     0x01    /* navigation system: GPS */
#define SYS_BDS     0x02    /* navigation system: BDS */
#define MAXSYS      2       /* max supported systems */

// refer to RINEX V3.05 --------------------------------------------
#define FREQ_L1     1575.42e6   /* GPS L1 */
#define FREQ_L2     1227.60e6   /* GPS L2 */
#define FREQ_L5     1176.45e6   /* GPS L5 */
#define FREQ_B1I    1561.098e6  /* BDS B1 */
#define FREQ_B1C    1575.42e6   /* BDS B1C / B1A */
#define FREQ_B2A    1176.45e6   /* BDS B2A */    
#define FREQ_B2     1207.140e6  /* BDS B2 / B2b */
#define FREQ_B2AB   1191.795e6  /* BDS B2(B2a + B2b) */
#define FREQ_B3I    1268.52e6   /* BDS B3 / B3A */ 

// satellites defination -------------------------------------------
#define MAXGPSSATS  32          /* maximum of GPS satellites */
#define MAXBDSSATS  64          /* maximum of BDS satellites */
#define SATS        (MAXGPSSATS + MAXBDSSATS)   /* total satellites */

// math defination -------------------------------------------------
#define PI          (atan(1) * 4)   /* pi 3.1415926.... */
#define VEL_LIGHT   299792458.0     /* speed of light */

// processing mode -------------------------------------------------
#define MODE_SINGLE  0
#define MODE_RTK     1

// frequency configuration
#define FREQTYPE_L1 0x01                /* frequency type: L1/B1 */
#define FREQTYPE_L2 0x02                /* frequency type: L2/B2 */
#define FREQTYPE_L3 0x04                /* frequency type: L5/L3 */
#define FREQTYPE_ALL 0xFF               /* frequency type: all */
#define MAXFREQ     3
// default paths ---------------------------------------------------
#define CONFPATH  "./config/"         /* configuration path */
#define RESPATH   "./result/"         /* result save path */
#define NAME_CONF "process.conf"      /* name of processing configuraion file */
#define NAME_SITE "sites.conf"        /* name of sites information file */

// observation defination ------------------------------------------
#define FREQCODE_GPS    "125"       /* GPS frequency in rinex 3.04 */
#define FREQCODE_BDS    "267"       /* BDS frequency in rinex 3.04 */
#define TRACKMODE       "CWI"       /* supported track mode */
#define OBSTYPE         "CLDS"      /* observation type */
#define SYSTEMS         "GC"        /* supported systems */

// ephemeris type --------------------------------------------------
#define EPH_PREC    0
#define EPH_BRDC    1

// time related ----------------------------------------------------
#define GPST2UTC   -18
#define BDT2GPST    14

#define MAXEPH      36

// satellite define ------------------------------------------------
#define MAXGPSSATS  32
#define MAXBDSSATS  64
#define MAXSTAS     (MAXGPSSATS + MAXBDSSATS)
#define MAXOBS      64

// max supported sites ---------------------------------------------
#define MAXSITES    2

const int SYS_ARRAY[] = {SYS_GPS, SYS_BDS};
const int FREQ_ARRAY[] = {FREQTYPE_L1, FREQTYPE_L2, FREQTYPE_L3};
const int GEO[] = {1, 2, 3, 4, 5, 59, 60, 61};


// ellipsoid type --------------------------------------------------
enum EllipsoidType { CGCS2000, WGS84 };   /* support WGS84 CGCS2000 */

// structure defination --------------------------------------------
struct prcopt {             /* processing options */
   int mode_;               /* mode for processing (MODE_???) */
   int navsys_;             /* systems to use */
   int freqtype_;           /* frequency to use */
   int ephtype_;            /* broadcast or precise eph to use */
   int clktype_;            /* clock type(broadcast or precise) */
   unsigned short freqnum_; /* number of used frequency */
   unsigned short nsys_;    /* number of systems */
   unsigned short sitenum_; /* number of site */
   unsigned short accel_output_; /* accel original output, 1 for delta, 2 for velocity */
   unsigned short gyro_output_;  /* gyro original output, 1 for delta, 2 for velocity */
   double gravity_;         /* local gravity used in INS */
   double align_time_;      /* INS align time (s) */
   double sampling_rate_;   /* INS sample rate */
   double init_pos_[3];     /* initial position in ecef (m) */
   double elecutoff_;       /* elevation cutoff (in radians) */
   double base_[3] = {0};   /* priori coordinates of base */
   double rover_[3] = {0};  /* priori coordinates of rover */
   double init_vel_[3];     /* init velocity(XYZ) */
   double lever_[3];        /* lever arm */
   double sigma_ra_;        /* 加速度计比力测量噪声的标准差 */
   double sigma_rg_;        /* 陀螺角速率测量噪声的标准差 */
   double sigma_bad_;       /* 加速度计动态零偏标准差 */
   double sigma_gbd_;       /* 陀螺动态零偏标准差 */
   double deltapos_flat_;   /* 经纬度方向位置误差方差 */
   double deltapos_vertical_;   /* 垂直方向位置误差方差 */
   double deltavel_flat_;       /* 水平方向速度误差方差 */
   double deltavel_vertical_;   /* 垂直方向速度误差方差 */
   double delta_pitch_;     /* 俯仰误差方差 */
   double delta_roll_;      /* 横滚角误差方差 */
   double delta_yaw_;       /* 航向角误差方差 */
   double delta_acc_;       /* 加速度计零偏误差方差 */
   double delta_gyro_;      /* 陀螺零偏误差方差 */
   string nbase_;           /* name of base */
   string nrover_;          /* name of rover */

public:
    ~prcopt();
};

// common functions ------------------------------------------------

/***********************************
 * transform radians to degree
 * @param   rad     [In]    rad
 * @return deg
***********************************/
double Rad2Deg(double rad);

/***********************************
 * transform degree to radians
 * @param   deg     [In]    degree
 * @return rad
***********************************/
double Deg2Rad(double deg);

#define Omega_e 7.292115E-5          //地球自转角速度
#define semi_major 6378137.0         //半长轴（m）
#define flattening 1.0/298.257222101 //扁率
const double e2 = 2 * flattening - flattening * flattening;//第一偏心率的平方

/*卯酉圈曲率半径（fai为纬度，单位rad）*/
double RN(double fai);
/*子午圈曲率半径（fai为纬度，单位rad）*/
double RM(double fai);
/*A function to build skew symmetric matrix(构造反对称阵)*/
Matrix3d SkewMat(Vector3d Vec);
/*
DR_inv
n系变量转换到llh的转换矩阵
llh_pos   LLH坐标系下的位置
*/
Matrix3d DR_inv(const Vector3d &llh_pos);


template <class T>
T str2num(string line) {
    stringstream buff;
    T num;
    buff << line; buff >> num;
    return num;
}

vector<double> WGS84_XYZ2BLH(const vector<double>& XYZ);

#endif // _COMMON_H_