#pragma once

/**
 * @PureINS.h 乞丐版 后续再完善命名和结构
 * @author Jianan Lou
 * @date 2021.6.15
 * @brief Header file of PureINS
 *
 *
 * @details
 * @see
 */

#ifndef PUREINS_H_
#define PUREINS_H_

#define Omega_e 7.292115E-5
#define WGS84_a 6378137.0
#define WGS84_b 6356752.3142451795
#define WGS84_f 1/298.257223563
#define WGS84_e2 0.0066943799901413
#define WGS84_GM 3.986004418e14
#define WGS84_Gamma_a 9.7803253359
#define WGS84_Gamma_b 9.8321849379

#include "..\..\Eigen\Dense"
#include<math.h>

double Cal_Gravity(double& B, double& H);

 
struct Accelerometer
{
	double f_x = 0.0;
	double f_y = 0.0;
	double f_z = 0.0;
};

struct Gyro
{
	double omega_x = 0.0;
	double omega_y = 0.0;
	double omega_z = 0.0;

};

struct Velocity
{
	double VN;
	double VE;
	double VD;
};

struct Euler
{
	double Roll;
	double Pitch;
	double Yaw;
};

struct quaternion
{
	double Q0;
	double Q1;
	double Q2;
	double Q3;

	void normalization()
	{
		double norm = sqrt(Q0*Q0 + Q1 * Q1 + Q2 * Q2 + Q3 * Q3);
		Q0 = Q0 / norm;
		Q1 = Q1 / norm;
		Q2 = Q2 / norm;
		Q3 = Q3 / norm;
	}
};

struct Attitude
{
	Euler E;
	quaternion Q;
};

struct Position
{
	double B;
	double L;
	double H;
};

struct IMR_Data_LJN//IMR数据结构体
{
	double _time = 0.0;//时间戳

	double _accel_x = 0.0;//x-加速度m/s^2
	double _accel_y = 0.0;//y-加速度m/s^2
	double _accel_z = 0.0;//z-加速度m/s^2

	double _gyro_x = 0.0;//x-角速度rad/s
	double _gyro_y = 0.0;//y-角速度rad/s
	double _gyro_z = 0.0;//z-角速度rad/s
};

struct INS_Result
{
	Velocity velocity;
	Attitude attitude;
	Position position;
};


class PureINS
{
public:
	PureINS() = default;

public:
	void Attitude_Updating(IMR_Data_LJN* imu_data_1, IMR_Data_LJN* imu_data_2, INS_Result* result_1, INS_Result* result_2);
	void Postion_Updating(IMR_Data_LJN* imu_data_1, IMR_Data_LJN* imu_data_2, INS_Result* result_1, INS_Result* result_2);
	void Velocity_Updating(IMR_Data_LJN* imu_data_1, IMR_Data_LJN* imu_data_2, INS_Result* result_1, INS_Result* result_2);


};



#endif PUREINS_H_