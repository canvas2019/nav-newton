#pragma once
#ifndef _DECODE_IMR_
#define _DECODE_IMR_
#define IMR_HEADER 512    // imr header is a total of 512 bytes long
#define IMR_RECORD 32     // imr record is a total of 32 bytes long epoch
#include "config.h"
#include <iomanip>
using namespace std;

/*------ imr header information ------*/
struct IMRHeader {
	bool _imr_header_status;//to judge whether the header is read or not. true if being read.
	char _szHeader[8];//NULL terminated ASCII string
	int8_t _bIsIntelOrMotorola;//0:Little Endian,1:Big Endian
	double _dVersionNumber;//Inertial Explorer program version number (e.g. 8.80)
	int32_t _bDeltaTheta;
	int32_t _bDeltaVelocity;
	double _dDataRateHz;
	double _dGyroScaleFactor;
	double _dAccelScaleFactor;
	int32_t _iUtcOrGpsTime;
	int32_t _iRcvTimeOrCorrTime;
	double _dTimeTagBias;
	char _szImuName[32];
	uint8_t _reserved1[4];
	char _szProgramName[32];
	char _tCreate[12];//should be time_type for 12 bytes
	bool _bLeverArmValid;//true if lever arms from IMU to primary GNSS antenna are stored in this header
	int32_t _lXoffset;
	int32_t _lYoffset;
	int32_t _lZoffset;
	int8_t _reserved2[354];
	IMRHeader(){
		_imr_header_status = false;
	}
};

/*------ imr record information ------*/
struct IMRRecord {
	double _time_sow;
	int32_t _gx;
	int32_t _gy;
	int32_t _gz; //scaled gyro measurement about the IMU Z - axis
	int32_t _ax;
	int32_t _ay;
	int32_t _az; //scaled accel measurement about the IMU Z - axis
};

/*------ raw IMU data ------*/
struct IMURAWData {
	double _time_sow;//time of the current measurement (second of week)
	double _gx;
	double _gy;
	double _gz;//gyro measurements (deg/s)
	double _ax;
	double _ay;
	double _az;//accel measurements (m/s2)
};

/*------ imr data ------*/
class IMRData
{
public:
	IMRHeader _imr_header;
	IMRRecord _imr_record;
	IMURAWData _imu_raw_data;
public:
	IMRData(FILE *_imr_fp);
	void decodeHeader(FILE *_imr_fp);
	void getIMURawData(FILE *_imr_fp);
	void decodeRecord(FILE *_imr_fp);
	void imrRecord2imuRawData();
	//~IMRData();
private:
};

/*------
test : decode imr and cout imu raw data  
should be deleted in use
------*/
bool test_decode(char*, char*);

#endif  // _DECODE_IMR_