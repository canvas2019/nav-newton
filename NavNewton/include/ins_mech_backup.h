/*----------------------------------------------------------
   ins_mech.h
   Created on 20 Jun 2021 DZ XIA
----------------------------------------------------------*/

#ifndef INS_MECH_H_
#define INS_MECH_H_

#include "..\..\Eigen\Dense"

// GRS80
#define a  6378137.0
#define e2  0.00669437999013
#define b  6356752.3141
#define GM  3.986005e+14
#define GammaA  9.7803267715
#define GammaB  9.8321863685
#define f  1.0 / 298.257222101

#define PI  3.1415926535898	// PI, provided by gnss ICD 
#define W_IE  7.2921151467e-5 // earth rotational angular rate
#define IMU_SAMPLE_PERIOD  0.01

#define m  (W_IE * W_IE * a * a * b / GM)

#define Bot(phi) (1-e2*sin(phi)*sin(phi))
#define SIN2(x) (sin(x)*sin(x))
#define COS2(x) (cos(x)*cos(x))
#define SQR(x) (x*x)

struct ImuData {
	double time;
	Eigen::Vector3d _gyro;
	Eigen::Vector3d _acce;
	bool _is_increment;	// whether the raw imu data is increment or rate
};

// INS解算结果
struct InsSol {
	double time;
	Eigen::Vector3d _pos;
	Eigen::Vector3d _vel;
	Eigen::Vector3d _att;	//姿态的欧拉角表示
	Eigen::Quaterniond q;	//姿态的四元数表示
};



//以下的更新都认为是对IMU输出是增量 进行更新
//顺序：速度更新-位置更新-姿态更新

/**
 * @brief  update attitude of epoch t_k according to pre&curr angular increment and attitude of epoch t_k-1
 * @note
 * @param  [in]		  pre_imu_data  : angular velocity increment of raw imu data at the previous moment
 *		   [in]		  curr_imu_data : angular velocity increment of raw imu data at the current moment
 *		   [in]		  pre_ins_sol   : INS solution results at the previous monent
 *		   [in & out] curr_ins_sol  : INS solution results at the current monent, input pos & vel and output att 
 * @retval void
 */
void MechAttitudeUpdate(const ImuData& pre_imu_data, const ImuData& curr_imu_data, const InsSol& pre_ins_sol,InsSol& curr_ins_sol);

/**
 * @brief  update velocity of epoch t_k according to pre&curr angular and acceleration increment
 * @note
 * @param  [in]	    pre_imu_data  : angular velocity and acceleration increment of raw imu data at the previous moment
 *		   [in]	    curr_imu_data : angular velocity and acceleration increment of raw imu data at the current moment
 *		   [in]	    pre_ins_sol   : INS solution results at the previous monent
 *		   [out]    curr_ins_sol  : INS solution results at the current monent, output velocity
 * @retval void
 */
void MechVelocityUpdate(const ImuData& pre_imu_data, const ImuData& curr_imu_data, const InsSol& pre_ins_sol, InsSol& curr_ins_sol);

/**
 * @brief  update position of epoch t_k according to velocity & position of previous epoch, and velocity of current epoch
 * @note
 * @param  [in]			pre_ins_sol   : INS solution results at the previous monent
 *		   [in & out]   curr_ins_sol  : INS solution results at the current monent, input velocity, output position
 * @retval void
 */
void MechPositionUpdate(const InsSol& pre_ins_sol, InsSol& curr_ins_sol);

//trans tools
Eigen::Matrix3d XdzQuaternionToRotationMatrix(const Eigen::Quaterniond& q);
Eigen::Vector3d XdzRotationMatrixToEuler(const Eigen::Matrix3d& rot_mat);
//--------------------------------机械编排内部使用的辅助函数-----------------------
/**
 * @brief  calculate RM(卯酉圈半径) according to local latitude
 * @note   
 * @param  [in]	 phi  : local latitude, units: rad
 * @retval double, value of RM, units: m
 */
double CalRM(double phi);

/**
 * @brief  calculate RN(子午圈半径) according to local latitude
 * @note
 * @param  [in]	 phi  : local latitude, units: rad
 * @retval double, value of RN, units: m
 */
double CalRN(double phi);

/**
 * @brief  calculate normal gravity according to local latitude and geodetic height
 * @note
 * @param  [in]	 phi  : local latitude, units: rad
 *         [in]  h    : geodetic height,units: m
 * @retval double, value of normal gravity, units: m/s^2
 */
double CalGravity(double phi, double h);

// Calculate the projection of gravity acceleration in nav frame
void Calg_ln(double lat, double h, Eigen::Vector3d& vecGn);
 
Eigen::Vector3d Cal_W_ie_n(const Eigen::Vector3d& pos);
Eigen::Vector3d Cal_W_en(const Eigen::Vector3d& vel, const Eigen::Vector3d& pos);

// calculate antisymmetric matrix of vector3d
Eigen::Matrix3d CalAntiSymMat(const Eigen::Vector3d& v);



#endif // INS_MECH_H_

