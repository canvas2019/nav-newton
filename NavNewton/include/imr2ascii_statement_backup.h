#pragma once
#include<string>

using namespace std;

/*-------------------
#类IMR仅支持有参构造，参数为所需要读取的文件名；
#类IMR的所有成员属性均为私有属性，提供了若干必要的数据获取接口，包括数据输出频率和IMR数据；
##获取IMR数据的接口重载，一个用于一次性获取所有数据，一个用于一次获取某一时刻的数据（按原文件数据排列顺序)；
#IMR类其他对外接口有文件读取和文件重写；
##文件读取：即将文件中的必要信息读入内存
##文件重写：将读入的数据按固定格式输出到用户指定文件
###输出格式：数据类型均为double，以11位字符输出，时间保留6位小数，惯性传感器数据保留5位小数；
###顺序为：time,gyro-x,gyro-y,gyro-z,accel-x,accel-y,accel-z；

                                                           ---by HC 2021/06/05
-------------------*/



struct IMR_Data//IMR数据结构体
{
    double _time = 0.0;//时间戳

    double _accel_x = 0.0;//x-加速度m/s^2
    double _accel_y = 0.0;//y-加速度m/s^2
    double _accel_z = 0.0;//z-加速度m/s^2

    double _gyro_x = 0.0;//x-角速度rad/s
    double _gyro_y = 0.0;//y-角速度rad/s
    double _gyro_z = 0.0;//z-角速度rad/s
};

class IMR
{
public:
    //操作
    int getIMR_Data(IMR_Data* idata);//获取imr数据，读取成功返回文件中的imr数据数量
    int getIMR_Data(IMR_Data* idata, int index);//获取某一历元的imr数据
    int IMR_ReadFile();//读取文件,读取成功返回文件中imr数据数量；无法打开文件或文件类型错误返回-1；文件读取过程出现故障返回0
    int IMR_RewriteASCII(string file_out_name);//将数据重写为ASCII文件
    
    double getDataRate()
    {
        return _data_rate;
    }

    IMR(string file_name)//构造函数
    {
        _file_name = file_name;
    }
    IMR(const IMR& imr0)//复制函数
    {
        _file_name = imr0._file_name;
        if (imr0._imr_data_num > 0)
        {
            _imr_data_num = imr0._imr_data_num;
            _imr_data = new IMR_Data[_imr_data_num];
            for (int i = 0; i < _imr_data_num; i++) _imr_data[i] = imr0._imr_data[i];
        }
        else  _imr_data_num = imr0._imr_data_num;
        _data_rate = imr0._data_rate;
        _accel_scale_factor = imr0._accel_scale_factor;
        _gyro_scale_factor = imr0._gyro_scale_factor;
    }
    ~IMR()//析构函数
    {
        if (_imr_data != NULL) delete[]_imr_data;
    }
private:
    //属性
    string _file_name = "0";//文件名
    IMR_Data* _imr_data = NULL;//惯性传感器数据
    double _data_rate = 0.0;//采样率Hz
    int _imr_data_num = 0;//文件中imr数据总数
    int32_t _is_delta_theata = 0;//陀螺仪输出数据：0~角速度；1~角度增量
    int32_t _is_delta_velocity = 0;//加表输出数据：0~加速度；1~速度增量
    double _accel_scale_factor = 0.0;//加表读书缩放因子
    double _gyro_scale_factor = 0.0;//陀螺仪读数缩放因子

    //操作
    int IMR_ReadHeader();//读取文件头
    int IMR_ReadData();//读取文件中的数据
};

int Imr2ASCII_TestHC();