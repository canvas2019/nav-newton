/*---------------------------------------------
   config.h
   create on 12 Apr 2021 ZHUOXU WHU
---------------------------------------------*/

#ifndef _CONFIG_H_
#define _CONFIG_H_
#include <fstream>
#include <iostream>
#include "common.h"
using namespace std;

class CConfig {
public: // constructors
	CConfig();
	CConfig(char* path);

public: // main functions
    /***************************************************
     * load user configurations
     * @param   [in]    path    configuration file path
     * @return
    ***************************************************/
    void LoadConfig(char* path);

    /**************************************************
     * set configurations to defaults
     * @return
    ***************************************************/
    void reset();

    /*************************************************
     * get configurations
     * @return configurations
    *************************************************/
    prcopt GetConf();
    
private: // processing configurations 
    void ParseConfLine(string line);    /* parse a line of process.conf */
    void Loadprcopt(char* path);    /* load process.conf */
    bool ParseLabel(string label, string value);  /* parse parameter */
    bool SetSys(string value);
    bool SetCutOff(string value);   // elevation cut-off
    bool SetMode(string value);     // processing mode
    bool SetEphType(string value);
    bool SetClkType(string value);
    bool SetFreq(string value);
    bool SetGravity(string label);
    bool SetAlignTime(string label);
    bool SetSampleRate(string label);
    bool SetInitialX(string label);
    bool SetInitialY(string label);
    bool SetInitialZ(string label);
    bool SetGyroOutMode(string label);
    bool SetAccelOutMode(string label);
    bool SetVx(string label);
    bool SetVy(string label);
    bool SetVz(string label);
    bool SetLeverForward(string label);
    bool SetLeverRight(string label);
    bool SetLeverDown(string label);
    bool SetSigmaRa(string label);
    bool SetSigmaRg(string label);
    bool SetSigmaBad(string label);
    bool SetSigmaBgd(string label);
    bool SetDeltaPosFlat(string label);
    bool SetDeltaPosVertical(string label);
    bool SetDeltaVertical(string label);
    bool SetDeltaVelFlat(string label);
    bool SetDeltaVelVertical(string label);
    bool SetDeltaPitch(string label);
    bool SetDeltaRoll(string label);
    bool SetDeltaYaw(string label);
    bool SetDeltaAcc(string label);
    bool SetDeltaGyro(string label);

private: // sites configurations
	void LoadSites(char* path);
	void ParseSiteLine(string line);

private:
	prcopt opt_;
};

#endif // _CONFIG_H_