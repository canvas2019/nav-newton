/*----------------------------------------------------------
   imr_converter.h 
   created on 1 Jun 2021 by dzXia WHU.sgg
   2021/06/06 last modified
----------------------------------------------------------*/

#ifndef IMR_CONVERTER_H_
#define IMR_CONVERTER_H_

// self-define time_type
struct time_type {
	int32_t tt;
	int32_t mon;
	int32_t year;
};

struct IMRHeaderXDZ {
	char _sz_header[8];	// fix string which is "$IMURAW\0"
	int8_t _is_intel_or_motorola;	// 0 = Intel(Little Endian),defualt
	double _version_number;	// IE program version number
	int32_t _delta_theta;	// 1 = default,data will be read as angular increments
	int32_t _delta_velocity;	// 1 = default,is the same as the above
	double _data_rate_hz;	// the data rate of IMU in Hz.
	double _gyro_scale_factor;	// gyro measurements should multiply this
	double _accel_scale_factor;	// is the same as the above
	int32_t _utc_or_gpstime;	// defines the time tag as gps or utc,0 = Unknown
	int32_t _rcvtime_or_corrtime;	// defines the time tag,0 = Unknown
	double _time_tag_bias;	// a known bias between gps and imu time
	char _sz_imu_name[32];	// name of the imu
	uint8_t _reserverd1[4];
	char _sz_program_name[32];	// name of calling program
	time_type _t_create;
	bool _leverarm_valid;	// true if the lever arm is stored in this header
	int32_t _x_offset;	// x value of the lever arm,in millimeters
	int32_t _y_offset;
	int32_t _z_offset;
	int8_t _reserved[354];
};

// .imr file converter,can convert imu file into imr file
// Example:
//    const string imu_filename = "xxxx.imu";
//    const string imr_filename = "xxxx.imr";
//	  IMRConvert imr_converter;
//	  imr_converter.setIMRHeader();
//    imr_converter.convertToIMR(imu_filename,imr_filename);
// Caution:
//    1.the units of Gyro is deg/s and the Accel is m/s^2 in 
//    whether the imu file or the imr file, and multiply a
//    scale factor.
//    2.the date rate of imu is 100Hz by default.
//    3.Just take care the units.
class IMRConverter {
public: // constructors
	IMRConverter() = default;

public: // functions
	/*******************************************************
	 * use default paras to set .imr file header
	 * @paras 
	 * @return
	*******************************************************/
	void setIMRHeader();
	
	/******************************************************* 
	 * read .imu file, convert and save as .imr file
	 * @paras [in] .imu fliepath [out] generated .imr filepath
	 * @return
	*******************************************************/
	void convertToIMR(const std::string& imu_filename, const std::string& imr_filename);

public:
	IMRHeaderXDZ _imr_header;
};
#endif IMR_CONVERTER_H_ // IMR_CONVERTER_H_