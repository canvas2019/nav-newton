/*---------------------------------------------
   navicommon.h
   created by GS
---------------------------------------------*/

#pragma once
#ifndef _LOOSE_COUPLED_
#define _LOOSE_COUPLED_
#include "config.h"
#include <../Eigen/Dense>
#include <../Eigen/Core>
using namespace Eigen;
using namespace std;

#define semi_major 6378137.0         //半长轴（m）
#define flattening 1.0/298.257222101 //扁率
/*6.21*/
#define OMIGAE 7.292115e-5           //地球自转角速度

/*
LooseCoupled KF

gyro and accel bias modeled as white moise

*/
class KF
{
private:
	prcopt _opt;
	double _g_noise_psd;//gyro measurement noise psd(3 axis independent and have equal variance)
	double _a_noise_psd;//accel measurement noise psd(3 axis independent and have equal variance)
	//gyro and accel bias Parameters: modeled as  white moise
	double _bgd_psd;//gyro dynamic bias psd(3 axis independent and have equal variance)
	double _bad_psd;//accel dynamic bias psd(3 axis independent and have equal variance)
private:
	Matrix<double, 15, 15> _F;//differential equation coefficient matrix
	Matrix<double, 15, 15> _S;//psd matrix for _Q:_S *_tor_s=_Q
public:
	//coefficient matrix
	double _tor_s;//propagation interval(s)
	Matrix<double, 15, 1> _x; //error state: pos,vel,att,bg,ba
	Matrix<double, 15, 15> _Phi;//transition matrix
	Matrix<double, 6, 1> _Z;//obs vector,noting that zero lever arm is assumed here
	Matrix<double, 6, 15> _H;//obs coefficient matrix
	// variance
	Matrix<double, 15, 15> _Q; // Q:system noise covariance matrix
	Matrix<double, 15, 15> _P; // P:error state variance matrix
	Matrix<double, 6, 6> _R; // R:measurement noise variance matrix
//methods
public:
	KF(prcopt opt);//constructed function
	void processPredict(const Vector3d &imu_pos, const Vector3d &imu_vel, const Vector3d &imu_eul, const Vector3d &f_bi_b, const double dT);
	void processUpdate(Vector3d &imu_pos, Vector3d &imu_vel, Vector3d &imu_eul, Vector3d &g_bias, Vector3d &a_bias, const Vector3d &gnss_pos, const Vector3d &gnss_vel, const Matrix3d &Dxx, const Matrix3d &Dvv);
private:
	void Init_P();//initial P
	void Init_S();//initial S
};


///*--------------------- common part -----------------------*/
///*卯酉圈曲率半径（fai为纬度，单位rad）*/
//double RN(double fai);
///*子午圈曲率半径（fai为纬度，单位rad）*/
//double RM(double fai);
///*A function to build skew symmetric matrix(构造反对称阵)*/
//Matrix3d SkewMat(Vector3d Vec);
/**********
calDCMbyEul
利用欧拉角向量Eul（横滚角、俯仰角、航向角：rad）计算方向余弦矩阵C（DCM）
**********/
bool calDCMbyEul(Matrix3d &DCM, const Vector3d &Eul);
/**********
calEulbyDCM
利用DCM求欧拉角向量Eul（横滚角、俯仰角、航向角：rad）
**********/
bool calEulbyDCM(Vector3d &Eul, const Matrix3d &DCM);
/**********
calQbyEul
利用欧拉角向量Eul（横滚角、俯仰角、航向角：rad）计算四元数
**********/
bool calQbyEul(Vector4d &_q, const Vector3d &Eul);
/*
索米利亚纳地球重力场模型
*/
double g0(double fai);
/*
参考椭球表面地心半径
*/
double ReS(double fai);
/*
correctLeverArm     杆臂改正：（GNSS天线相位中心->INS中心）
*/
void correctLeverArm(Vector3d &gnss_pos, Vector3d &gnss_vel, const Vector3d &imu_pos, const Vector3d &imu_vel, const Vector3d &imu_eul, const Vector3d &w_bi_b, const Vector3d &lever_arm);

#endif  // _LOOSE_COUPLED_