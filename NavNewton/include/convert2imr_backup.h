#pragma once
/**
 * @convert2imr.h
 * @author Jianan Lou
 * @date 2021.6.3
 * @brief Header file of convert2imr. Other file formats can be converted to IMR format. 
 *
 *
 * @details
 * @see
 */

#ifndef CONVERT2IMR_H_
#define CONVERT2IMR_H_

#include <fstream>


/* 12 bytes. The concrete form remains to be explored. */
typedef struct {
	int32_t year;
	int32_t mon;
	int32_t day;
}imutime;

typedef struct {
	char _header[8];	          /* ��$IMURAW\0�� �C NULL terminated ASCII string */
	int8_t _is_intel_or_motorola; /* 0 = Intel(Little Endian),defualt 1 = Motorola (Big Endian) */
	double _version_number;	      /* Inertial Explorer program version number (e.g. 8.80) */

	int32_t _delta_theta;	      /* 0 = Data to follow will be read as scaled angular rates 
							         1 = (default), data to follow will be read as delta thetas, 
	                                 meaning angular increments(i.e.scale and multiply 
	                                 by dDataRateHz to getdegrees / second) */

	int32_t _delta_velocity;	  /* 0 = Data to follow will be read as scaled accelerations
	                                 1 = (default), data to follow will be read as delta velocities, meaning
		                             velocity increments(i.e.scale and multiply by dDataRateHz to get m/s2) */

	double _data_rate;	          /* The data rate of the IMU in Hz. e.g. 0.01 second data rate is 100 HZ */

	double _gyro_scale_factor;	  /* If bDeltaTheta == 0, multiply the gyro measurements by this to get degrees/second
		                             If bDeltaTheta == 1, multiply the gyro measurements by this to get degrees,
	                                 then multiply by dDataRateHz to get degrees / second */

	double _accel_scale_factor;   /* If bDeltaVelocity == 0, multiply the accel measurements by this to get m/s2
		                             If bDeltaVelocity == 1, multiply the accel measurements by this to get m/s,
	                                 then multiply by dDataRateHz to get m/s2 */

	int32_t _utc_or_gpstime;	  /* Defines the time tags as GPS or UTC seconds of the week
	                                 0 = Unknown, will default to GPS
	                                 1 = Time tags are UTC seconds of week
		                             2 = Time tags are GPS seconds of week */

	int32_t _rcvtime_or_corrtime; /* Defines whether the time tags are on the nominal top of the second or are corrected for receiver time bias
		                             0 = Unknown, will default to corrected time
		                             1 = Time tags are top of the second
		                             2 = Time tags are corrected for receiver clock bias */

	double _time_tag_bias;	      /* If you have a known bias between your GPS and IMU time tags enter it here */
	char _imu_name[32];	          /* Name of the IMU being used */
	uint8_t _reserved1[4];        /* Reserved for future use */
	char _program_name[32];	      /* Name of calling program */
	imutime _t_create;               /* Creation time of file */
	bool _leverarm_valid;	      /* True if lever arms from IMU to primary GNSS antenna are stored in this header */
	int32_t _x_offset;	          /* X value of the lever arm, in millimeters */
	int32_t _y_offset;            /* Y value of the lever arm, in millimeters */
	int32_t _z_offset;            /* Z value of the lever arm, in millimeters */
	int8_t _reserved[354];        /* Reserved for future use */
}IMRHeader_LJN;


/* Function: Other file formats convert to IMR format. Currently, only IMU to IMR format is realized. */
/***********************e.g.****************************
    const char* imu_filename = "xxx.imu";
    const char* imr_filename = "xxx.imr";
    IMRConvert imr_converter;
    imr_converter.setIMRHeader();
    imr_converter.IMU2IMR(imu_filename,imr_filename); 
*************************e.g.***************************/

class IMRConverter_LJN{
public:
	IMRConverter_LJN() = default;


/*!
 *  @brief  Set the header of IMR file
 *
 *
 *  @param[in]   none
 *
 *  @param[out]  none
 * 
 *  @return      void
 */
	void initIMRHeader();

/*!
 *  @brief  Write the header of IMR file
 *
 *
 *  @param[in]  outfile         Pointer to the output file
 *
 *  @param[in]  none
 *
 *  @return     void
 */
	void writeIMRHeader(std::ofstream& outfile);

/*!
 *  @brief  Conversion from IMU format to IMR format
 *
 *
 *  @param[in]  imu_filename	 The file name of IMU format data
 *
 *  @param[in]  imr_filename     The file name of IMR format data
 *
 *  @return     True/False       Conversion success is True, conversion failure is False
 */
	bool IMU2IMR(const char* imu_filename, const char* imr_filename);

public:
	IMRHeader_LJN _imr_header;
};

#endif CONVERT2IMR_H_