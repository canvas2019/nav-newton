#include"PureINS.h"
using namespace Eigen;


void Quaternion_Calculation(quaternion O, quaternion P, quaternion Q, quaternion& R);
double CalRM(double& B);
double CalRN(double& B);
double R8(unsigned char* buff);

/*注意：输入的需要是增量*/
void PureINS::Attitude_Updating(IMR_Data_LJN* imu_data_1, IMR_Data_LJN* imu_data_2, INS_Result* result_1, INS_Result* result_2)
{
	/*别忘了变成增量*/

	Vector3d delta_Thetak(imu_data_2->_gyro_x, imu_data_2->_gyro_y, imu_data_2->_gyro_z);
	Vector3d delta_Thetak_1(imu_data_1->_gyro_x, imu_data_1->_gyro_y, imu_data_1->_gyro_z);

	Vector3d Phi_k = delta_Thetak + (1.0 / 12.0)*delta_Thetak_1.cross(delta_Thetak);
	double norm_Phik = Phi_k.norm();

	if (abs(norm_Phik) < 1e-9) {//如果陀螺输出为0，则直接返回上一时刻姿态
		result_2->attitude.E.Roll = result_1->attitude.E.Roll;
		result_2->attitude.E.Pitch = result_1->attitude.E.Pitch;
		result_2->attitude.E.Yaw = result_1->attitude.E.Yaw;
		return;
	}

	quaternion qbk_bk_1;
	qbk_bk_1.Q0 = cos(0.5*norm_Phik);
	qbk_bk_1.Q1 = sin(0.5*norm_Phik) / (0.5*norm_Phik)*0.5*Phi_k(0);
	qbk_bk_1.Q2 = sin(0.5*norm_Phik) / (0.5*norm_Phik)*0.5*Phi_k(1);
	qbk_bk_1.Q3 = sin(0.5*norm_Phik) / (0.5*norm_Phik)*0.5*Phi_k(2);

	double RN = CalRN(result_1->position.B);
	double RM = CalRM(result_1->position.B);

	Vector3d Omegaen_n(result_1->velocity.VE / (RN + result_1->position.H),
		-result_1->velocity.VN / (RM + result_1->position.H),
		-result_1->velocity.VE*tan(result_1->position.B) / (RN + result_1->position.H));
	Vector3d Omegaie_n(Omega_e*cos(result_1->position.B), 0, -Omega_e * sin(result_1->position.B));
	Vector3d Zeta = (Omegaen_n + Omegaie_n)*(imu_data_2->_time- imu_data_1->_time);

	double norm_Zeta = Zeta.norm();
	quaternion qnk_1_nk;
	qnk_1_nk.Q0 = cos(0.5*norm_Zeta);
	qnk_1_nk.Q1 = -sin(0.5*norm_Zeta) / (0.5*norm_Zeta)*0.5*Zeta(0);
	qnk_1_nk.Q2 = -sin(0.5*norm_Zeta) / (0.5*norm_Zeta)*0.5*Zeta(1);
	qnk_1_nk.Q3 = -sin(0.5*norm_Zeta) / (0.5*norm_Zeta)*0.5*Zeta(2);

	quaternion qbk_nk = { 0 };
	Quaternion_Calculation(qnk_1_nk, result_1->attitude.Q, qbk_bk_1, qbk_nk);
	qbk_nk.normalization();

	result_2->attitude.Q = qbk_nk; //四元数

	MatrixXd Cbn = MatrixXd::Zero(3, 3);
	Cbn(0, 0) = result_2->attitude.Q.Q0*result_2->attitude.Q.Q0
		+ result_2->attitude.Q.Q1*result_2->attitude.Q.Q1
		- result_2->attitude.Q.Q2*result_2->attitude.Q.Q2
		- result_2->attitude.Q.Q3*result_2->attitude.Q.Q3;
	Cbn(0, 1) = 2 * (result_2->attitude.Q.Q1*result_2->attitude.Q.Q2 - result_2->attitude.Q.Q0*result_2->attitude.Q.Q3);
	Cbn(0, 2) = 2 * (result_2->attitude.Q.Q1*result_2->attitude.Q.Q3 + result_2->attitude.Q.Q0*result_2->attitude.Q.Q2);
	Cbn(1, 0) = 2 * (result_2->attitude.Q.Q1*result_2->attitude.Q.Q2 + result_2->attitude.Q.Q0*result_2->attitude.Q.Q3);
	Cbn(1, 1) = result_2->attitude.Q.Q0*result_2->attitude.Q.Q0
		      - result_2->attitude.Q.Q1*result_2->attitude.Q.Q1
		+ result_2->attitude.Q.Q2*result_2->attitude.Q.Q2
		- result_2->attitude.Q.Q3*result_2->attitude.Q.Q3;
	Cbn(1, 2) = 2 * (result_2->attitude.Q.Q2*result_2->attitude.Q.Q3 - result_2->attitude.Q.Q0*result_2->attitude.Q.Q1);
	Cbn(2, 0) = 2 * (result_2->attitude.Q.Q1*result_2->attitude.Q.Q3 - result_2->attitude.Q.Q0*result_2->attitude.Q.Q2);
	Cbn(2, 1) = 2 * (result_2->attitude.Q.Q2*result_2->attitude.Q.Q3 + result_2->attitude.Q.Q0*result_2->attitude.Q.Q1);
	Cbn(2, 2) = result_2->attitude.Q.Q0*result_2->attitude.Q.Q0
		- result_2->attitude.Q.Q1*result_2->attitude.Q.Q1
		- result_2->attitude.Q.Q2*result_2->attitude.Q.Q2
		+ result_2->attitude.Q.Q3*result_2->attitude.Q.Q3;

	/*result_2->attitude.E.Roll  = atan2(2.0 * (qbk_nk.Q0*qbk_nk.Q1 + qbk_nk.Q2*qbk_nk.Q3), 1.0 - 2.0*(qbk_nk.Q1*qbk_nk.Q1 + qbk_nk.Q2*qbk_nk.Q2));
	result_2->attitude.E.Pitch =  asin(2.0 * (qbk_nk.Q0*qbk_nk.Q2 - qbk_nk.Q1*qbk_nk.Q3));
	result_2->attitude.E.Yaw   = atan2(2.0 * (qbk_nk.Q0*qbk_nk.Q3 + qbk_nk.Q1*qbk_nk.Q2), 1.0 - 2.0 * (qbk_nk.Q2*qbk_nk.Q2 + qbk_nk.Q3*qbk_nk.Q3));*/

	result_2->attitude.E.Roll = atan2(Cbn(2, 1), Cbn(2, 2));
	result_2->attitude.E.Pitch = atan(-Cbn(2, 0) / (sqrt(Cbn(2, 1)*Cbn(2, 1) + Cbn(2, 2)*Cbn(2, 2))));
	result_2->attitude.E.Yaw = atan2(Cbn(1, 0), Cbn(0, 0));

	//欧拉角
	/*cout <<  "t="  << setw(10) << right << fixed << setprecision(3) << result_2->t << ' ';
	cout << "Roll=" <<  setw(18) << right << fixed << setprecision(14) << result_2->attitude.E.Roll/Pi *180 << ' ' ;
	cout << "Pitch=" << setw(18) << right << fixed << setprecision(14) << result_2->attitude.E.Pitch/Pi*180 << ' ';
	cout << "Yaw=" << setw(18) << right << fixed << setprecision(14) << result_2->attitude.E.Yaw/Pi*180 << ' ';*/
}

/*注意：输入的需要是增量*/
void  PureINS::Postion_Updating(IMR_Data_LJN* imu_data_1, IMR_Data_LJN* imu_data_2, INS_Result* result_1, INS_Result* result_2)
{
	Vector3d delta_Vk(imu_data_2->_accel_x, imu_data_2->_accel_y, imu_data_2->_accel_z);
	Vector3d delta_Thetak(imu_data_2->_gyro_x, imu_data_2->_gyro_y, imu_data_2->_gyro_z);

	Vector3d delta_Vk_1(imu_data_1->_accel_x, imu_data_1->_accel_y, imu_data_1->_accel_z);
	Vector3d delta_Thetak_1(imu_data_1->_gyro_x, imu_data_1->_gyro_y, imu_data_1->_gyro_z);

	Vector3d delta_v_bk_1 = delta_Vk + 0.5*delta_Thetak.cross(delta_Vk) + 1.0 / 12.0*(delta_Thetak_1.cross(delta_Vk) + delta_Vk_1.cross(delta_Thetak));

	double RN = CalRN(result_1->position.B);
	double RM = CalRM(result_1->position.B);
	Vector3d Omegaen_n(result_1->velocity.VE / (RN + result_1->position.H),
		-result_1->velocity.VN / (RM + result_1->position.H),
		-result_1->velocity.VE*tan(result_1->position.B) / (RN + result_1->position.H));
	Vector3d Omegaie_n(Omega_e*cos(result_1->position.B), 0, -Omega_e * sin(result_1->position.B));

	Vector3d Zeta_k_1_k = (Omegaen_n + Omegaie_n)*(imu_data_2->_time - imu_data_2->_time);

	MatrixXd Zeta_k_1_k_Antisymmetrical = MatrixXd::Zero(3, 3);
	Zeta_k_1_k_Antisymmetrical(0, 1) = -Zeta_k_1_k(2); Zeta_k_1_k_Antisymmetrical(0, 2) = Zeta_k_1_k(1);
	Zeta_k_1_k_Antisymmetrical(1, 0) = Zeta_k_1_k(2);  Zeta_k_1_k_Antisymmetrical(1, 2) = -Zeta_k_1_k(0);
	Zeta_k_1_k_Antisymmetrical(2, 0) = -Zeta_k_1_k(1); Zeta_k_1_k_Antisymmetrical(2, 1) = Zeta_k_1_k(0);

	MatrixXd Cbk_1_nk_1 = MatrixXd::Zero(3, 3);

	Cbk_1_nk_1(0, 0) = cos(result_1->attitude.E.Pitch)*cos(result_1->attitude.E.Yaw);
	Cbk_1_nk_1(0, 1) = -cos(result_1->attitude.E.Roll)*sin(result_1->attitude.E.Yaw)
		+ sin(result_1->attitude.E.Roll)*sin(result_1->attitude.E.Pitch)*cos(result_1->attitude.E.Yaw);
	Cbk_1_nk_1(0, 2) = sin(result_1->attitude.E.Roll)*sin(result_1->attitude.E.Yaw)
		+ cos(result_1->attitude.E.Roll)*sin(result_1->attitude.E.Pitch)*cos(result_1->attitude.E.Yaw);
	Cbk_1_nk_1(1, 0) = cos(result_1->attitude.E.Pitch)*sin(result_1->attitude.E.Yaw);
	Cbk_1_nk_1(1, 1) = cos(result_1->attitude.E.Roll)*cos(result_1->attitude.E.Yaw)
		+ sin(result_1->attitude.E.Roll)*sin(result_1->attitude.E.Pitch)*sin(result_1->attitude.E.Yaw);
	Cbk_1_nk_1(1, 2) = -sin(result_1->attitude.E.Roll)*cos(result_1->attitude.E.Yaw)
		+ cos(result_1->attitude.E.Roll)*sin(result_1->attitude.E.Pitch)*sin(result_1->attitude.E.Yaw);
	Cbk_1_nk_1(2, 0) = -sin(result_1->attitude.E.Pitch);
	Cbk_1_nk_1(2, 1) = sin(result_1->attitude.E.Roll)*cos(result_1->attitude.E.Pitch);
	Cbk_1_nk_1(2, 2) = cos(result_1->attitude.E.Roll)*cos(result_1->attitude.E.Pitch);

	MatrixXd delta_v_bk_1_ = MatrixXd::Zero(3, 1);
	delta_v_bk_1_(0, 0) = delta_v_bk_1(0);
	delta_v_bk_1_(1, 0) = delta_v_bk_1(1);
	delta_v_bk_1_(2, 0) = delta_v_bk_1(2);

	MatrixXd I = MatrixXd(3, 3);
	I << 1, 0, 0,
		 0, 1, 0,
		 0, 0, 1;
	MatrixXd deltaV_n = (I - (0.5*Zeta_k_1_k_Antisymmetrical))*Cbk_1_nk_1*delta_v_bk_1_;
	Vector3d gl_n(0, 0, Cal_Gravity(result_1->position.B, result_1->position.H));
	Vector3d Vk_1_n(result_1->velocity.VN, result_1->velocity.VE, result_1->velocity.VD);

	Vector3d deltaVg_cor_k_n = (gl_n - (2 * Omegaie_n + Omegaen_n).cross(Vk_1_n))*(imu_data_2->_time - imu_data_1->_time);

	Vector3d deltaV_n_(deltaV_n(0, 0), deltaV_n(1, 0), deltaV_n(2, 0));

	Vector3d Vkn = Vk_1_n + deltaV_n_ + deltaVg_cor_k_n;

	result_2->velocity.VN = Vkn(0);
	result_2->velocity.VE = Vkn(1);
	result_2->velocity.VD = Vkn(2);

	/*cout << "VN=" << setw(12) << right << fixed << setprecision(8) << result_2->velocity.VN << ' ';
	cout << "VE=" << setw(12) << right << fixed << setprecision(8) << result_2->velocity.VE << ' ';
	cout << "VD=" << setw(12) << right << fixed << setprecision(8) << result_2->velocity.VD << ' ';*/
}


/*注意：输入的需要是增量*/
void  PureINS::Velocity_Updating(IMR_Data_LJN* imu_data_1, IMR_Data_LJN* imu_data_2, INS_Result* result_1, INS_Result* result_2)
{
	result_2->position.H = result_1->position.H - 0.5*(result_2->velocity.VD + result_1->velocity.VD)*(imu_data_2->_time - imu_data_1->_time);

	double RM = CalRM(result_1->position.B);
	double Ave_H = 0.5*(result_2->position.H + result_1->position.H);

	result_2->position.B = result_1->position.B + 0.5*(result_2->velocity.VN + result_1->velocity.VN) / (RM + Ave_H)*(imu_data_2->_time - imu_data_1->_time);

	double Ave_B = 0.5*(result_2->position.B + result_1->position.B);
	double RN = CalRN(Ave_B);
	result_2->position.L = result_1->position.L + 0.5*(result_2->velocity.VE + result_1->velocity.VE) / ((RN + Ave_H)*cos(Ave_B))*(imu_data_2->_time - imu_data_1->_time);

	/*cout << "B=" << setw(10) << right << fixed << setprecision(8) << result_2->position.B/Pi*180 << ' ';
	cout << "L=" << setw(10) << right << fixed << setprecision(8) << result_2->position.L/Pi*180 << ' ';
	cout << "H=" << setw(8)  << right << fixed << setprecision(4) << result_2->position.H << endl;*/
}


void Quaternion_Calculation(quaternion O, quaternion P, quaternion Q, quaternion& R)
{
	MatrixXd o = MatrixXd::Zero(4, 4);
	MatrixXd p = MatrixXd::Zero(4, 1);
	MatrixXd o_p = MatrixXd::Zero(4, 1);
	MatrixXd o_p_ = MatrixXd::Zero(4, 4);
	MatrixXd q = MatrixXd::Zero(4, 1);
	MatrixXd r = MatrixXd::Zero(4, 1);

	for (int i = 0; i < 4; i++)
		o(i, i) = O.Q0;

	o(0, 1) = -O.Q1; o(0, 2) = -O.Q2; o(0, 3) = -O.Q3;
	o(1, 0) = O.Q1; o(1, 2) = -O.Q3; o(1, 3) = O.Q2;
	o(2, 0) = O.Q2; o(2, 1) = O.Q3; o(2, 3) = -O.Q1;
	o(3, 0) = O.Q3; o(3, 1) = -O.Q2; o(3, 2) = O.Q1;

	p(0, 0) = P.Q0; p(1, 0) = P.Q1; p(2, 0) = P.Q2; p(3, 0) = P.Q3;

	o_p = o * p;

	for (int k = 0; k < 4; k++)
	{
		o_p_(k, k) = o_p(0, 0);
	}

	o_p_(0, 1) = -o_p(1, 0); o_p_(0, 2) = -o_p(2, 0); o_p_(0, 3) = -o_p(3, 0);
	o_p_(1, 0) = o_p(1, 0); o_p_(1, 2) = -o_p(3, 0); o_p_(1, 3) = o_p(2, 0);
	o_p_(2, 0) = o_p(2, 0); o_p_(2, 1) = o_p(3, 0); o_p_(2, 3) = -o_p(1, 0);
	o_p_(3, 0) = o_p(3, 0); o_p_(3, 1) = -o_p(2, 0); o_p_(3, 2) = o_p(1, 0);

	q(0, 0) = Q.Q0; q(1, 0) = Q.Q1; q(2, 0) = Q.Q2; q(3, 0) = Q.Q3;

	r = o_p_ * q;

	R.Q0 = r(0, 0);  R.Q1 = r(1, 0);  R.Q2 = r(2, 0); R.Q3 = r(3, 0);
}

double CalRM(double& B)
{
	double a;
	double e2, RM;
	a = WGS84_a;
	e2 = WGS84_e2;
	RM = a * (1 - e2) / pow((1 - e2 * sin(B)*sin(B)), 1.5);
	return RM;
}

double CalRN(double& B)
{
	double a = 6378137.0;
	double e2, RN;
	e2 = WGS84_e2;
	RN = a / sqrt(1 - e2 * sin(B)*sin(B));
	return RN;
}

double Cal_Gravity(double& B, double& H)
{
	double Gamma_Phi = (WGS84_a * WGS84_Gamma_a*cos(B)* cos(B) + WGS84_b * WGS84_Gamma_b * sin(B) * sin(B))
		/ sqrt(WGS84_a * WGS84_a * cos(B) * cos(B) + WGS84_b * WGS84_b * sin(B) * sin(B));

	double m = Omega_e * Omega_e * WGS84_a * WGS84_a * WGS84_b / WGS84_GM;
	double Gravity = Gamma_Phi * (1 - 2 / WGS84_a * (1 + WGS84_f + m - 2 * WGS84_f*sin(B)*sin(B))*H + 3 / (WGS84_a*WGS84_a)*H*H);
	return Gravity;
}

double R8(unsigned char* buff)
{
	double r;
	memcpy(&r, buff, 8);
	return r;
}

