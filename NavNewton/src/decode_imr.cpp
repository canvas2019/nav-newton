#include "decode_imr.h"

/*------constructing IMRData,decode imr header------*/
IMRData::IMRData(FILE *_imr_fp)
{
	if (!_imr_header._imr_header_status)decodeHeader(_imr_fp);
}

/*------decode imr header informaton------*/
void IMRData::decodeHeader(FILE *_imr_fp)
{
	char buff[IMR_HEADER];
	if (fread(buff, IMR_HEADER, 1, _imr_fp) < 1){
		cerr << "read imr header in file error! " << endl;
		_imr_header._imr_header_status = false;
		exit(-1);//read imr header to buff; exit if error
	}
	memcpy(&_imr_header._szHeader, buff, 8);
	memcpy(&_imr_header._bIsIntelOrMotorola, buff+8, 1);
	memcpy(&_imr_header._dVersionNumber, buff + 9, 8);
	memcpy(&_imr_header._bDeltaTheta, buff + 17, 4);
	memcpy(&_imr_header._bDeltaVelocity, buff + 21, 4);
	memcpy(&_imr_header._dDataRateHz, buff + 25, 8);
	memcpy(&_imr_header._dGyroScaleFactor, buff + 33, 8);
	memcpy(&_imr_header._dAccelScaleFactor, buff + 41, 8);
	memcpy(&_imr_header._iUtcOrGpsTime, buff + 49, 4);
	memcpy(&_imr_header._iRcvTimeOrCorrTime, buff + 53, 4);
	memcpy(&_imr_header._dTimeTagBias, buff + 57, 8);
	memcpy(&_imr_header._szImuName, buff + 65, 32);
	memcpy(&_imr_header._reserved1, buff + 97, 4);
	memcpy(&_imr_header._szProgramName, buff + 101, 32);
	memcpy(&_imr_header._tCreate, buff + 133, 12);
	memcpy(&_imr_header._bLeverArmValid, buff + 145, 1);
	memcpy(&_imr_header._lXoffset, buff + 146, 4);
	memcpy(&_imr_header._lYoffset, buff + 150, 4);
	memcpy(&_imr_header._lZoffset, buff + 154, 4);
	memcpy(&_imr_header._reserved2, buff + 158, 354);
	_imr_header._imr_header_status = true;
}

/*------ get imu raw data by decode imr record and trans it ------*/
void IMRData::getIMURawData(FILE * _imr_fp)
{
	decodeRecord(_imr_fp);
	imrRecord2imuRawData();
}

/*------ decode imr record information for imu raw data ------*/
void IMRData::decodeRecord(FILE * _imr_fp)
{
	if (!_imr_header._imr_header_status) {
		cerr << "imr header has not been read! " << endl;
		exit(-1);//imr header has not been read; exit if error
	}
	char buff[IMR_RECORD];
	if (fread(buff, IMR_RECORD, 1, _imr_fp) < 1) {
		cerr << "read imr record in file error! " << endl;
		throw "read imr record in file error! ";//read imr record to buff; exit if error
		return;
	}
	memcpy(&_imr_record._time_sow, buff, 8);
	memcpy(&_imr_record._gx, buff + 8, 4);
	memcpy(&_imr_record._gy, buff + 12, 4);
	memcpy(&_imr_record._gz, buff + 16, 4);
	memcpy(&_imr_record._ax, buff + 20, 4);
	memcpy(&_imr_record._ay, buff + 24, 4);
	memcpy(&_imr_record._az, buff + 28, 4);
}

/*------ trans imr record information to imu raw data------*/
void IMRData::imrRecord2imuRawData()
{
	if (_imr_header._iUtcOrGpsTime == 1)
	{
		_imu_raw_data._time_sow = _imr_record._time_sow + 18;//GPST-UTC=18s
	}
	else
	{
		_imu_raw_data._time_sow = _imr_record._time_sow;
	}
	/*-- imr record in deg/s and m/s2 --*/
	if (_imr_header._bDeltaTheta == 0) {
		_imu_raw_data._gx = _imr_record._gx*_imr_header._dGyroScaleFactor;
		_imu_raw_data._gy = _imr_record._gy*_imr_header._dGyroScaleFactor;
		_imu_raw_data._gz = _imr_record._gz*_imr_header._dGyroScaleFactor;
		_imu_raw_data._ax = _imr_record._ax*_imr_header._dAccelScaleFactor;
		_imu_raw_data._ay = _imr_record._ay*_imr_header._dAccelScaleFactor;
		_imu_raw_data._az = _imr_record._az*_imr_header._dAccelScaleFactor;
	}
	/*-- imr record in deg and m/s --*/
	else {
		_imu_raw_data._gx = _imr_record._gx*_imr_header._dGyroScaleFactor*_imr_header._dDataRateHz;
		_imu_raw_data._gy = _imr_record._gy*_imr_header._dGyroScaleFactor*_imr_header._dDataRateHz;
		_imu_raw_data._gz = _imr_record._gz*_imr_header._dGyroScaleFactor*_imr_header._dDataRateHz;
		_imu_raw_data._ax = _imr_record._ax*_imr_header._dAccelScaleFactor*_imr_header._dDataRateHz;
		_imu_raw_data._ay = _imr_record._ay*_imr_header._dAccelScaleFactor*_imr_header._dDataRateHz;
		_imu_raw_data._az = _imr_record._az*_imr_header._dAccelScaleFactor*_imr_header._dDataRateHz;
	}
}

/*------
test : decode imr and cout imu raw data
shoulde be deleted in use
------*/
bool test_decode(char* _imr_path, char* _imu_path)
{
	/*--- test get imu data ---*/

	FILE* _imr_fp;//imr file temp
	errno_t err = fopen_s(&_imr_fp, _imr_path, "rb");//以二进制的方式打开imr文件，若打开成功返回0
	if (err != 0) {
		cerr << "Cannot open imr bin file.\n";
		exit(-1);
	}
	ofstream _imu_wrt(_imu_path, ios::trunc);//结果文本文件写入流
	_imu_wrt << setw(15) << setiosflags(ios::right) << setprecision(6) << "time"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "Gyro X"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "Gyro Y"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "Gyro Z"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "Accel X"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "Accel Y"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "Accel Z"
		<< "\n"
		<< setw(15) << setiosflags(ios::right) << setprecision(6) << "(sow)"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "(deg/s)"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "(deg/s)"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "(deg/s)"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "(m/s2)"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "(m/s2)"
		<< setw(10) << setiosflags(ios::right) << setprecision(5) << "(m/s2)"
		<< "\n";
	IMRData _imr_data(_imr_fp);
	while (!feof(_imr_fp))
	{
		_imr_data.getIMURawData(_imr_fp);
		_imu_wrt << setw(15) << setiosflags(ios::fixed) << setiosflags(ios::right) << setprecision(6) << _imr_data._imu_raw_data._time_sow
			<< setw(10) << setiosflags(ios::fixed) << setprecision(ios::right) << setprecision(5) << _imr_data._imu_raw_data._gx
			<< setw(10) << setiosflags(ios::fixed) << setprecision(ios::right) << setprecision(5) << _imr_data._imu_raw_data._gy
			<< setw(10) << setiosflags(ios::fixed) << setprecision(ios::right) << setprecision(5) << _imr_data._imu_raw_data._gz
			<< setw(10) << setiosflags(ios::fixed) << setprecision(ios::right) << setprecision(5) << _imr_data._imu_raw_data._ax
			<< setw(10) << setiosflags(ios::fixed) << setprecision(ios::right) << setprecision(5) << _imr_data._imu_raw_data._ay
			<< setw(10) << setiosflags(ios::fixed) << setprecision(ios::right) << setprecision(5) << _imr_data._imu_raw_data._az
			<< setw(10) << setiosflags(ios::fixed) << setprecision(ios::right) << setprecision(5) << "\n";
	}
	fclose(_imr_fp);
	_imu_wrt.close();
	return true;
}
