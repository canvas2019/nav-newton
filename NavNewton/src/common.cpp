#include "common.h"
#include <string.h>
#include <stdio.h>

prcopt::~prcopt() {
    memset(this, 0, sizeof(prcopt));
}

double Deg2Rad(double deg) {
    return (deg * PI) / 180.0;
}

double Rad2Deg(double rad) {
    return rad * 180.0 / PI;
}

/*--------------------- common part -----------------------*/
/*卯酉圈曲率半径（fai为纬度，单位rad）*/
double RN(double fai)
{
	double W = sqrt(1.0 - e2 * sin(fai) * sin(fai));
	double RN = semi_major / W;
	return RN;
}

/*子午圈曲率半径（fai为纬度，单位rad）*/
double RM(double fai)
{
	double W = sqrt(1.0 - e2 * sin(fai) * sin(fai));
	double RM = semi_major * (1.0 - e2) / (W*W*W);
	return RM;
}

/*A function to build skew symmetric matrix(构造反对称阵)*/
Matrix3d SkewMat(Vector3d Vec)
{
	Matrix3d Skew = Matrix3d::Zero(3, 3);
	Skew(0, 1) = -Vec(2); Skew(0, 2) = Vec(1);
	Skew(1, 0) = Vec(2); Skew(1, 2) = -Vec(0);
	Skew(2, 0) = -Vec(1); Skew(2, 1) = Vec(0);
	return Skew;
}

/*
DR_inv
n系变量转换到llh的转换矩阵
llh_pos   LLH坐标系下的位置:纬度、经度(rad)、高程
*/
Matrix3d DR_inv(const Vector3d &llh_pos)
{
	double lat = llh_pos(0);
	double lon = llh_pos(1);
	double height = llh_pos(2);
	Matrix3d mat_n2l = Matrix3d::Zero(3, 3);//将北东地转化为LLH系
	mat_n2l(0, 0) = 1.0 / (RM(lat) + height);
	mat_n2l(1, 1) = 1.0 / ((RN(lat) + height)*cos(lat));
	mat_n2l(2, 2) = -1.0;
	return mat_n2l;
}

vector<double> WGS84_XYZ2BLH(const vector<double>& XYZ)
{
	if (XYZ.size() != 3) {
		cerr << "Error! WGS84_XYZ2BLH, XYZ.size() != 3" << endl;
		exit(0);
	}
	const double e_sq = 0.00669437999013;
	const double a = 6.3781370e6;

	double X = XYZ[0];
	double Y = XYZ[1];
	double Z = XYZ[2];

	double L = atan2(Y, X);

	//需要迭代计算，初始deltaZ = e_sq*Z
	double deltaZ = e_sq * Z;
	double B0 = atan2(Z + deltaZ, sqrt(X * X + Y * Y));
	double B = B0;
	double sqrt_XYZdeltaZ = sqrt(X * X + Y * Y + (Z + deltaZ)*(Z + deltaZ));
	double sinB = (Z + deltaZ) / sqrt_XYZdeltaZ;
	double N = a / sqrt(1 - e_sq * sinB*sinB);
	double H = sqrt_XYZdeltaZ - N;

	//开始迭代
	double thresh = 1e-14;
	for (int i = 0; i < 20; i++) {
		deltaZ = N * e_sq*sinB;
		B = atan2(Z + deltaZ, sqrt(X * X + Y * Y));
		sqrt_XYZdeltaZ = sqrt(X * X + Y * Y + (Z + deltaZ)*(Z + deltaZ));
		sinB = (Z + deltaZ) / sqrt_XYZdeltaZ;
		N = a / sqrt(1 - e_sq * sinB*sinB);
		H = sqrt_XYZdeltaZ - N;
		if (fabs(B - B0) < thresh) {
			break;
		}
		else {
			B0 = B;
		}
	}
	vector<double> res{ B,L,H };
	return res;
}
