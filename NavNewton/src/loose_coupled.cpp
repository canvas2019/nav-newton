#include "loose_coupled.h"

void KF::Init_P()
{
	_P.setZero();
	_P(0, 0) = _opt.deltapos_flat_;	_P(1, 1) = _opt.deltapos_flat_;
	_P(2, 2) = _opt.deltapos_vertical_;
	_P(3, 3) = _opt.deltavel_flat_;	_P(4, 4) = _opt.deltavel_flat_;	_P(5, 5) = _opt.deltavel_flat_;
	_P(6, 6) = _opt.delta_roll_; _P(7, 7) = _opt.delta_pitch_; _P(8, 8) = _opt.delta_yaw_;
	_P(9,9) = _opt.delta_gyro_; _P(10, 10) = _opt.delta_gyro_; _P(11, 11) = _opt.delta_gyro_;
	_P(12, 12) = _opt.delta_acc_; _P(13, 13) = _opt.delta_acc_; _P(14, 14) = _opt.delta_acc_;
}

void KF::Init_S()
{
	double SAMPLE_INTERVAL = 1.0 / _opt.sampling_rate_;
	_S.setZero();
	_g_noise_psd = _opt.sigma_rg_ * _opt.sigma_rg_*SAMPLE_INTERVAL;// gyro measurement noise psd(3 axis independent and have equal variance)
    _a_noise_psd = _opt.sigma_ra_ *  _opt.sigma_ra_*SAMPLE_INTERVAL;//accel measurement noise psd(3 axis independent and have equal variance)
	//gyro and accel bias Parameters: modeled as  white moise
	_bgd_psd = _opt.sigma_gbd_ * _opt.sigma_gbd_ / SAMPLE_INTERVAL;//gyro dynamic bias psd(3 axis independent and have equal variance)
	_bad_psd = _opt.sigma_bad_ * _opt.sigma_bad_ / SAMPLE_INTERVAL;//accel dynamic bias psd(3 axis independent and have equal variance)


	Matrix3d I3 = MatrixXd::Identity(3, 3);
	_S.block<3, 3>(3, 3) = _a_noise_psd * I3;
	_S.block<3, 3>(6, 6) = _g_noise_psd * I3;
	_S.block<3, 3>(9, 9) = _bgd_psd * I3;
	_S.block<3, 3>(12, 12) = _bad_psd * I3;
}
/*
Kf constructed function
clear _x
init _P
init _S
*/
//KF::KF()
//{
//	_x.setZero();
//	/*----set P----*/
//	Init_P();
//	/*----set S----*/
//	Init_S();
//}

KF::KF(prcopt opt)
{
	memcpy(&_opt, &opt, sizeof(prcopt));
	_x.setZero();
	/*----set P----*/
	Init_P();
	/*----set S----*/
	Init_S();
}

/*
KF process predict
input:
imu_pos     LLH coor by imu mechanical layout
imu_vel     NED velocity by imu mechanical layout
imu_eul     eul
f_bi_b      accel measurments
dT          propagation interval(s)
output:
_Q          system noise covariance matrix
_P          error state variance matrix
*/
void KF::processPredict(const Vector3d &imu_pos, const Vector3d &imu_vel, const Vector3d &imu_eul, const Vector3d &f_bi_b, const double dT)
{
	double L = imu_pos(0);
	double lambda = imu_pos(1);
	double h = imu_pos(2);
	_tor_s = dT;

	double vN = imu_vel(0);
	double vE = imu_vel(1);
	double vD = imu_vel(2);

	double _RM = RM(L);
	double _RN = RN(L);

	double _sinL = sin(L);
	double _cosL = cos(L);
	double _tanL = tan(L);
	/*----------------------------------------- differential equation ----------------------------------------------*/
	/*----- cal _F -----*/
	_F.setZero();

	/*-位置三行-------------------------------------------------------*/
	Matrix3d F12 = Matrix3d::Zero(3, 3);
	F12(0, 0) = 1.0 / (_RM + h);
	F12(1, 1) = 1.0 / ((_RN + h)*_cosL);
	F12(2, 2) = -1.0;
	_F.block<3, 3>(0, 3) = F12;
	/*--高阶项--------------------------*/
	Matrix3d F11 = Matrix3d::Zero(3, 3);
	F11(0, 2) = -1.0*vN / (pow(_RM + h, 2));
	F11(1, 0) = vE * _sinL / ((_RN + h)*_cosL*_cosL);
	F11(1, 2) = -1.0*vE / (pow(_RN + h, 2)*_cosL);
	_F.block<3, 3>(0, 0) = F11;

	/*-速度三行-------------------------------------------------------*/
	Matrix3d F21 = Matrix3d::Zero(3, 3);
	F21(2, 2) = -2.0*g0(L) / ReS(L);
	_F.block<3, 3>(3, 0) = F21;
	Matrix3d F23 = Matrix3d::Zero(3, 3);
	Matrix3d C_b2n;
	calDCMbyEul(C_b2n, imu_eul);
	Vector3d f_bi_n = C_b2n * f_bi_b;
	F23 = -1.0 * SkewMat(f_bi_n);
	_F.block<3, 3>(3, 6) = F23;
	Matrix3d F25 = C_b2n;
	_F.block<3, 3>(3, 12) = F25;
	/*-高阶项------------------*/
	Matrix3d F22 = Matrix3d::Zero(3, 3);
	F22(0, 0) = vD / (_RM + h);
	F22(0, 1) = -2.0*vE*_tanL / (_RN + h) - 2.0* OMIGAE*_sinL;
	F22(0, 2) = vN / (_RM + h);
	F22(1, 0) = vE * _tanL / (_RN + h) + 2.0*OMIGAE*_sinL;
	F22(1, 1) = (vN*_tanL + vD) / (_RN + h);
	F22(1, 2) = vE / (_RN + h) + 2.0*OMIGAE*_cosL;
	F22(2, 0) = -2.0*vN / (_RM + h);
	F22(2, 1) = -2.0*vE / (_RN + h) - 2 * OMIGAE*_cosL;
	F22(2, 2) = 0.0;
	_F.block<3, 3>(3, 3) = F22;

	/*-姿态三行--------------------------------------------------------*/
	Matrix3d F32 = Matrix3d::Zero(3, 3);
	F32(0, 1) = -1.0 / (_RN + h);
	F32(1, 0) = 1.0 / (_RM + h);
	F32(2, 1) = _tanL / (_RN + h);
	_F.block<3, 3>(6, 3) = F32;
	Matrix3d F34 = C_b2n;
	_F.block<3, 3>(6, 9) = F34;
	/*-高阶项----*/
	Matrix3d F33 = Matrix3d::Zero(3, 3);
	Vector3d w_ei_n;//地球自转角速度在n系下的投影
	Vector3d w_ne_n;//n系相对于e系的角速度在n系下的投影（牵连角速度）
	Vector3d w_ni_n;//n系相对于i系的角速度在n系下的投影（牵连角速度）
	w_ei_n << OMIGAE * _cosL, 0, -1.0*OMIGAE * _sinL;
	w_ne_n << vE / (_RN + h), -1.0*vN / (_RM + h), -1.0*vE*_tanL / (_RN + h);
	w_ni_n = w_ei_n + w_ne_n;
	F33 = -1.0*SkewMat(w_ni_n);
	_F.block<3, 3>(6, 6) = F33;
	Matrix3d F31 = Matrix3d::Zero(3, 3);
	F31(0, 0) = OMIGAE * _sinL;
	F31(0, 2) = vE / pow(_RN + h, 2);
	F31(1, 2) = -1.0*vN / pow(_RM + h, 2);
	F31(2, 0) = OMIGAE * _cosL + vE / ((_RN + h)*_cosL*_cosL);
	F31(2, 2) = -vE * _tanL / pow(_RN + h, 2);
	_F.block<3, 3>(6, 0) = F31;

	/*------------------零偏误差部分的建模-----------------*/
	/*按课本来:10-15行全为0*/

	/*----------------------------------------- system equation ----------------------------------------------*/
	/*---- cal Phi----*/
	_Phi = MatrixXd::Identity(15, 15) + _F * _tor_s;

	/*----  predict  ----*/
	_x.setZero();
	/*---- cal Q----*/
	_Q = _S * _tor_s;
	/*---- cal P----*/
	//_P = _Phi * _P*_Phi.transpose() + _Q;
	_P = _Phi * (_P + _Q / 2.0)*_Phi.transpose() + _Q / 2.0;//课本(3.46）
}
/*
KF process update
input:
imu_pos     LLH coor by imu mechanical layout
imu_vel     NED velocity by imu mechanical layout
imu_eul     eul
g_bias      gyro bias
a_bias      accel bias
gnss_pos    LLH coor by GNSS(leverarm corrected)
gnss_vel    NED vel  by GNSS(leverarm corrected)
Dxx         LLH coor by GNSS (leverarm corrected) variance matrix
Dvv         NED vel  by GNSS (leverarm corrected) variance matrix
output:
corrected:
imu_pos     LLH coor by imu mechanical layout
imu_vel     NED velocity by imu mechanical layout
imu_eul     eul
g_bias      gyro bias
a_bias      accel bias
*/
void KF::processUpdate(Vector3d &imu_pos, Vector3d &imu_vel, Vector3d &imu_eul, Vector3d &g_bias, Vector3d &a_bias, const Vector3d &gnss_pos, const Vector3d &gnss_vel, const Matrix3d &Dxx, const Matrix3d &Dvv)
{
	/*----------------------------------------- observation equation ----------------------------------------------*/
	_Z.block<3, 1>(0, 0) = imu_pos - gnss_pos;//注意杆臂问题
	_Z.block<3, 1>(3, 0) = imu_vel - gnss_vel;
	_H.setZero();
	_H.block<6, 6>(0, 0) = MatrixXd::Identity(6, 6);
	/*---- cal R ----*/
	_R.setZero();
	_R.block<3, 3>(0, 0) = Dxx;
	_R.block<3, 3>(3, 3) = Dvv;

	/*----  update  ----*/
	Matrix<double, 15, 15>I = MatrixXd::Identity(15, 15);
	Matrix<double, 15, 6> K = _P * _H.transpose()*((_H*_P*_H.transpose() + _R).inverse());
	Matrix<double, 6, 1> V = _Z - _H * _x;

	_x = _x + K * V; 

	_P = (I - K * _H)*_P*((I - K * _H).transpose()) + K * _R*K.transpose();
	/*--- back error ---*/
	Vector3d _pos_error = _x.block<3, 1>(0, 0);
	Vector3d _vel_error = _x.block<3, 1>(3, 0);
	Vector3d _att_error = _x.block<3, 1>(6, 0);
	Vector3d _g_bias_error = _x.block<3, 1>(9, 0);
	Vector3d _a_bias_error = _x.block<3, 1>(12, 0);
	/*---- according matlab ----*/
	imu_pos = imu_pos - _pos_error;
	imu_vel = imu_vel - _vel_error;
	/*---- att is different ----*/
	//方法1
	Matrix3d _delta_C_b2n;
	Matrix3d _C_b2n_old;
	calDCMbyEul(_delta_C_b2n, _att_error);
	calDCMbyEul(_C_b2n_old, imu_eul);
	Matrix3d _C_n2b_new = _C_b2n_old.transpose()*_delta_C_b2n;
	Matrix3d _C_b2n_new = _C_n2b_new.transpose();
	calEulbyDCM(imu_eul, _C_b2n_new);
	//方法2
	//Matrix3d _delta_C_b2n;
	//Matrix3d _C_b2n_old;
	//_delta_C_b2n = Matrix3d::Identity(3, 3) + SkewMat(_att_error);
	//calDCMbyEul(_C_b2n_old, imu_eul);
	//Matrix3d _C_n2b_new = _C_b2n_old.transpose()*_delta_C_b2n;
	//Matrix3d _C_b2n_new = _C_n2b_new.transpose();
	//calEulbyDCM(imu_eul, _C_b2n_new);

	/*for bias is different*/
	g_bias = g_bias + _g_bias_error;
	a_bias = a_bias + _a_bias_error;
}





///*--------------------- common part -----------------------*/
///*卯酉圈曲率半径（fai为纬度，单位rad）*/
//double RN(double fai)
//{
//	double W = sqrt(1.0 - e2 * sin(fai) * sin(fai));
//	double RN = semi_major / W;
//	return RN;
//}
//
///*子午圈曲率半径（fai为纬度，单位rad）*/
//double RM(double fai)
//{
//	double W = sqrt(1.0 - e2 * sin(fai) * sin(fai));
//	double RM = semi_major * (1.0 - e2) / (W*W*W);
//	return RM;
//}
//
///*A function to build skew symmetric matrix(构造反对称阵)*/
//Matrix3d SkewMat(Vector3d Vec)
//{
//	Matrix3d Skew = Matrix3d::Zero(3, 3);
//	Skew(0, 1) = -Vec(2); Skew(0, 2) = Vec(1);
//	Skew(1, 0) = Vec(2); Skew(1, 2) = -Vec(0);
//	Skew(2, 0) = -Vec(1); Skew(2, 1) = Vec(0);
//	return Skew;
//}

/*
correctLeverArm     杆臂改正：（GNSS天线相位中心->INS中心）

gnss_pos    gnss解算纬经高
gnss_vel    gnss解算北东地速度
imu_pos     imu机械编排纬经高
imu_vel     imu机械编排北东地速度
imu_eul     imu机械编排姿态角(roll pitch yaw :rad)
w_bi_b      陀螺三轴输出（rad/s）
lever_arm   b系下的三轴杆臂值
*/
void correctLeverArm(Vector3d &gnss_pos, Vector3d &gnss_vel, const Vector3d &imu_pos, const Vector3d &imu_vel, const Vector3d &imu_eul, const Vector3d &w_bi_b, const Vector3d &lever_arm)
{
	double lat = imu_pos(0);
	double lon = imu_pos(1);
	double height = imu_pos(2);
	double vN = imu_vel(0);
	double vE = imu_vel(1);
	double vD = imu_vel(2);
	//double lat = gnss_pos(0);
	//double lon = gnss_pos(1);
	//double height = gnss_pos(2);
	//double vN = gnss_vel(0);
	//double vE = gnss_vel(1);
	//double vD = gnss_vel(2);
	double _RM = RM(lat);//子午圈曲率半径
	double _RN = RN(lat);//卯酉圈曲率半径
	Vector3d eul = imu_eul;//获取姿态角
	Vector3d w_ei_n;//地球自转角速度在n系下的投影
	Vector3d w_ne_n;//n系相对于e系的角速度在n系下的投影（牵连角速度）
	Vector3d w_ni_n;//n系相对于i系的角速度在n系下的投影（牵连角速度）
	w_ei_n << OMIGAE * cos(lat), 0, -1.0*OMIGAE * sin(lat);
	w_ne_n << vE / (_RN + height), -1.0*vN / (_RM + height), -1.0*vE*tan(lat) / (_RN + height);
	w_ni_n = w_ei_n + w_ne_n;
	Matrix3d mat_n2l = Matrix3d::Zero(3, 3);//将北东地转化为LLH系
	mat_n2l(0, 0) = 1.0 / (_RM + height);
	mat_n2l(1, 1) = 1.0 / ((_RN + height)*cos(lat));
	mat_n2l(2, 2) = -1.0;
	Matrix3d C_b2n = Matrix3d::Zero(3, 3);//b系到n系的DCM
	calDCMbyEul(C_b2n, imu_eul);
	Matrix3d OMIGA_ni_n = SkewMat(w_ni_n);
	Matrix3d OMIGA_bi_b = SkewMat(w_bi_b);
	/*位置改正*/
	gnss_pos = gnss_pos - mat_n2l * C_b2n*lever_arm;
	/*速度改正*/
	gnss_vel = gnss_vel - C_b2n * OMIGA_bi_b*lever_arm + OMIGA_ni_n * C_b2n*lever_arm;
}







/**********
calDCMbyEul

利用欧拉角向量Eul（横滚角、俯仰角、航向角：rad）计算方向余弦矩阵C（DCM）
**********/
bool calDCMbyEul(Matrix3d &DCM, const Vector3d &Eul)
{
	Vector4d q;//姿态四元数
	double phi = Eul(0);
	double theta = Eul(1);
	double psi = Eul(2);
	q(0) = cos(0.5*phi)*cos(0.5*theta)*cos(0.5*psi) + sin(0.5*phi)*sin(0.5*theta)*sin(0.5*psi);
	q(1) = sin(0.5*phi)*cos(0.5*theta)*cos(0.5*psi) - cos(0.5*phi)*sin(0.5*theta)*sin(0.5*psi);
	q(2) = cos(0.5*phi)*sin(0.5*theta)*cos(0.5*psi) + sin(0.5*phi)*cos(0.5*theta)*sin(0.5*psi);
	q(3) = cos(0.5*phi)*cos(0.5*theta)*sin(0.5*psi) - sin(0.5*phi)*sin(0.5*theta)*cos(0.5*psi);
	double q_bk_nk_mod;//当前时刻姿态四元数的模
	q_bk_nk_mod = sqrt(q(0)*q(0) + q(1)*q(1) + q(2)*q(2) + q(3)*q(3));
	q = q / q_bk_nk_mod;
	DCM(0, 0) = q(0)*q(0) + q(1)*q(1) - q(2)*q(2) - q(3)*q(3);
	DCM(0, 1) = 2.0*(q(1)*q(2) - q(0)*q(3));
	DCM(0, 2) = 2.0*(q(1)*q(3) + q(0)*q(2));
	DCM(1, 0) = 2.0*(q(1)*q(2) + q(0)*q(3));
	DCM(1, 1) = q(0)*q(0) - q(1)*q(1) + q(2)*q(2) - q(3)*q(3);
	DCM(1, 2) = 2.0*(q(2)*q(3) - q(0)*q(1));
	DCM(2, 0) = 2.0*(q(1)*q(3) - q(0)*q(2));
	DCM(2, 1) = 2.0*(q(2)*q(3) + q(0)*q(1));
	DCM(2, 2) = q(0)*q(0) - q(1)*q(1) - q(2)*q(2) + q(3)*q(3);
	return true;
}

/**********
calEulbyDCM

利用DCM求欧拉角向量Eul（横滚角、俯仰角、航向角：rad）
**********/
bool calEulbyDCM(Vector3d &Eul, const Matrix3d &DCM)
{
	Eul(0) = atan2(DCM(2, 1), DCM(2, 2));
	Eul(1) = atan(-DCM(2, 0) / sqrt(pow(DCM(2, 1), 2) + pow(DCM(2, 2), 2)));
	Eul(2) = atan2(DCM(1, 0), DCM(0, 0));
	return true;
}

/**********
calQbyEul

利用欧拉角向量Eul（横滚角、俯仰角、航向角：rad）计算四元数
**********/
bool calQbyEul(Vector4d &_q, const Vector3d &Eul)
{
	Vector4d q;//姿态四元数
	double phi = Eul(0);
	double theta = Eul(1);
	double psi = Eul(2);
	q(0) = cos(0.5*phi)*cos(0.5*theta)*cos(0.5*psi) + sin(0.5*phi)*sin(0.5*theta)*sin(0.5*psi);
	q(1) = sin(0.5*phi)*cos(0.5*theta)*cos(0.5*psi) - cos(0.5*phi)*sin(0.5*theta)*sin(0.5*psi);
	q(2) = cos(0.5*phi)*sin(0.5*theta)*cos(0.5*psi) + sin(0.5*phi)*cos(0.5*theta)*sin(0.5*psi);
	q(3) = cos(0.5*phi)*cos(0.5*theta)*sin(0.5*psi) - sin(0.5*phi)*sin(0.5*theta)*cos(0.5*psi);
	double q_bk_nk_mod;//当前时刻姿态四元数的模
	q_bk_nk_mod = sqrt(q(0)*q(0) + q(1)*q(1) + q(2)*q(2) + q(3)*q(3));
	q = q / q_bk_nk_mod;
	_q = q;
	return true;
}

/*
索米利亚纳地球重力场模型
*/
double g0(double fai)
{
	double g0 = 9.7803253359*(1 + 0.001931853*sin(fai)*sin(fai)) / sqrt(1 - e2 * sin(fai)*sin(fai));
	return g0;
}

/*
参考椭球表面地心半径
*/
double ReS(double fai)
{
	double ReS = RN(fai)*sqrt(cos(fai)*cos(fai) + (1 - e2)*(1 - e2)*sin(fai)*sin(fai));
	return ReS;
}