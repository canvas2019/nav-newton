#include "ins_mech_backup.h"

using namespace Eigen;

void MechAttitudeUpdate(const ImuData& pre_imu_data, const ImuData& curr_imu_data, const InsSol& pre_ins_sol, InsSol& curr_ins_sol)
{
	//Judging whether IMU output is incremental or not
	//TODO
	//The following default imu output is incremental
	
	Eigen::Vector3d vel_ave = 0.5 * (pre_ins_sol._vel + curr_ins_sol._vel);
	Eigen::Vector3d pos_ave = 0.5 * (pre_ins_sol._pos + curr_ins_sol._pos);
	Eigen::Vector3d W_en = Cal_W_en(vel_ave, pos_ave);
	Eigen::Vector3d W_ie_n = Cal_W_ie_n(pos_ave);

	Quaterniond q_b;	// Quaternion bk to bk-1
	Quaterniond q_n;	// Quaternion nk-1 to nk

	// Coordinate System b update
	// Equivalent rotation vector phi
	Eigen::Vector3d phi = curr_imu_data._gyro + pre_imu_data._gyro.cross(curr_imu_data._gyro) / 12.0;
	double phi_b_norm = phi.norm();
	double wb = cos(0.5 * phi_b_norm);
	double xb = sin(0.5 * phi_b_norm) / (0.5 * phi_b_norm) * 0.5 * phi(0);
	double yb = sin(0.5 * phi_b_norm) / (0.5 * phi_b_norm) * 0.5 * phi(1);
	double zb = sin(0.5 * phi_b_norm) / (0.5 * phi_b_norm) * 0.5 * phi(2);
	q_b = Quaterniond(wb, xb, yb, zb);

	// Coordinate System n update
	// Equivalent rotation vector phi_n
	Eigen::Vector3d phi_n = (W_en + W_ie_n) * IMU_SAMPLE_PERIOD;
	double phi_n_norm = phi_n.norm();
	double wn = cos(0.5 * phi_n_norm);
	double xn = -sin(0.5 * phi_n_norm) / (0.5 * phi_n_norm) * 0.5 * phi_n(0);
	double yn = -sin(0.5 * phi_n_norm) / (0.5 * phi_n_norm) * 0.5 * phi_n(1);
	double zn = -sin(0.5 * phi_n_norm) / (0.5 * phi_n_norm) * 0.5 * phi_n(2);
	q_n = Quaterniond(wn, xn, yn, zn);

	curr_ins_sol.q = q_n * pre_ins_sol.q * q_b;
	curr_ins_sol.q.normalize(); // normalization
	curr_ins_sol._att = XdzRotationMatrixToEuler(XdzQuaternionToRotationMatrix(curr_ins_sol.q));
		
}

void MechVelocityUpdate(const ImuData& pre_imu_data, const ImuData& curr_imu_data, const InsSol& pre_ins_sol, InsSol& curr_ins_sol)
{
	Eigen::Vector3d v_fk_n, v_gcor, g_ln;
	Eigen::Vector3d v_fk_b;
	Eigen::Matrix3d C_bn0, part1;
	

	Eigen::Vector3d w_en_n = Cal_W_en(pre_ins_sol._vel, pre_ins_sol._pos);
	Eigen::Vector3d w_ie_n = Cal_W_ie_n(pre_ins_sol._pos);
	Calg_ln(pre_ins_sol._pos(0), pre_ins_sol._pos(2), g_ln);

	//t_k-1 epoch's v,g_ln,w
	v_gcor = (g_ln - (2 * w_ie_n + w_en_n).cross(pre_ins_sol._vel)) * IMU_SAMPLE_PERIOD;
	Eigen::Vector3d phi = (w_ie_n + w_en_n) * IMU_SAMPLE_PERIOD;
	part1 = Eigen::Matrix3d::Identity() - 0.5 * CalAntiSymMat(phi);
	v_fk_b = curr_imu_data._acce + 0.5 * (curr_imu_data._gyro.cross(curr_imu_data._acce))
		+ (pre_imu_data._gyro.cross(curr_imu_data._acce) + pre_imu_data._acce.cross(curr_imu_data._gyro)) / 12.0;
	
	C_bn0 = XdzQuaternionToRotationMatrix(pre_ins_sol.q);

	v_fk_n = part1 * C_bn0 * v_fk_b;
	
	curr_ins_sol._vel = pre_ins_sol._vel + v_fk_n + v_gcor;
}

void MechPositionUpdate(const InsSol& pre_ins_sol, InsSol& curr_ins_sol)
{
	double RM = CalRM(pre_ins_sol._pos(0));
	double RN = CalRN(pre_ins_sol._pos(0));
	
	curr_ins_sol._pos(2) = pre_ins_sol._pos(2) - 0.5 * (pre_ins_sol._vel(2) + curr_ins_sol._vel(2)) * IMU_SAMPLE_PERIOD;
	double h_ave = 0.5 * (pre_ins_sol._pos(2) + curr_ins_sol._pos(2));
	double vN_ave = 0.5 * (pre_ins_sol._vel(0) + curr_ins_sol._vel(0));
	double vE_ave = 0.5 * (pre_ins_sol._vel(1) + curr_ins_sol._vel(1));

	curr_ins_sol._pos(0) = pre_ins_sol._pos(0) + vN_ave / (RM + h_ave) * IMU_SAMPLE_PERIOD; //check: rectify the sequence of lat_ave and curr_ins_sol._pos(0)
	double lat_ave = 0.5 * (pre_ins_sol._pos(0) + curr_ins_sol._pos(0));
	RN = CalRN(lat_ave);
	curr_ins_sol._pos(1) = pre_ins_sol._pos(1) + vE_ave / ((RN + h_ave) * cos(lat_ave)) * IMU_SAMPLE_PERIOD;
}

//checked
double CalRM(double phi)
{
	return a * (1 - e2) / pow(Bot(phi), 1.5);
}

//checked
double CalRN(double phi)
{
	return a / sqrt(Bot(phi));
}

//checked
Eigen::Vector3d Cal_W_ie_n(const Eigen::Vector3d& pos)
{
	//before wN = w*sinphi
	double wN = W_IE * cos(pos(0));
	double wE = 0.0;
	double wU = -W_IE * sin(pos(0));
	return Eigen::Vector3d(wN, wE, wU);
}

//checked
//phi为纬度，h是大地高
double CalGravity(double phi, double h)
{
	double gamma_phi = (a * GammaA * COS2(phi) + b * GammaB * SIN2(phi)) / sqrt(a * a * COS2(phi) + b * b * SIN2(phi));
	double g = gamma_phi * (1 - 2 * h * (1 + f + m - 2 * f * SIN2(phi)) / a + 3 * h * h / (a * a));
	return g;
}

//checked
//计算重力加速度在n系的投影,在某个时刻的纬度lat 和高程h 下
void Calg_ln(double lat, double h, Eigen::Vector3d& vecGn)
{
	double g = CalGravity(lat, h);
	vecGn(0) = 0.0;
	vecGn(1) = 0.0;
	vecGn(2) = g; //check:rectify from -g to g
}

//checked
Eigen::Vector3d Cal_W_en(const Eigen::Vector3d& vel, const Eigen::Vector3d& pos)
{
	double RM = CalRM(pos(0));
	double RN = CalRN(pos(0));
	double wN = vel(1) / (RN + pos(2));
	double wE = -vel(0) / (RM + pos(2));
	double wU = -vel(1) * tan(pos(0)) / (RN + pos(2));
	return Eigen::Vector3d(wN, wE, wU);
}

Eigen::Matrix3d CalAntiSymMat(const Eigen::Vector3d& v)
{
	Eigen::Matrix3d ma;
	ma << 0, -v(2), v(1),
		v(2), 0, -v(0),
		-v(1), v(0), 0;
	return ma;
}

Eigen::Matrix3d XdzQuaternionToRotationMatrix(const Eigen::Quaterniond& q)
{
	double q0 = q.w();
	double q1 = q.x();
	double q2 = q.y();
	double q3 = q.z();

	double c11 = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3;
	double c12 = 2 * (q1 * q2 - q0 * q3);
	double c13 = 2 * (q1 * q3 + q0 * q2);
	double c21 = 2 * (q1 * q2 + q0 * q3);
	double c22 = q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3;
	double c23 = 2 * (q2 * q3 - q0 * q1);
	double c31 = 2 * (q1 * q3 - q0 * q2);
	double c32 = 2 * (q2 * q3 + q0 * q1);
	double c33 = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3;

	Eigen::Matrix3d DCM;
	DCM << c11, c12, c13,
		c21, c22, c23,
		c31, c32, c33;
	return DCM;
}

Vector3d XdzRotationMatrixToEuler(const Matrix3d& rot_mat)
{
	Eigen::Vector3d euler_angle;
	euler_angle(0) = atan2(rot_mat(2, 1), rot_mat(2, 2));
	euler_angle(1) = atan2(-rot_mat(2, 0), sqrt(rot_mat(2, 1) * rot_mat(2, 1) + rot_mat(2, 2) * rot_mat(2, 2)));
	euler_angle(2) = atan2(rot_mat(1, 0), rot_mat(0, 0));
	return euler_angle;
}