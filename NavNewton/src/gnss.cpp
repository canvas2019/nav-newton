#include"gnss.h"
#include<fstream>
#include<vector>
#include<string>
#include<iomanip>

void splitString(const  string& s, vector<string>& v)
{
	v.clear();
	int i = 0, j;
	while (i < s.size())
	{
		j = i;
		while (j < s.size() && s[j] == ' ') j++;
		i = j;
		while (i < s.size() && s[i] != ' ') i++;
		if (i != j) v.push_back(s.substr(j, i - j));
	}
}
void GNSS::readFile(string name)
{
	ifstream infile;
	infile.open(name, ios::in);

	if (!infile.is_open())
		cout << "Open file failure" << endl;
	string line;

	do
	{
		getline(infile, line);
	} while (line[0] != 'W');
	getline(infile, line);

	//double t, B, L, H, vE, vN, vU, sigmaB, sigmaL, sigmaH, sigmavE, sigmavN, sigmavU, q;
	int size = 0;
	vector<string> temp;
	while (getline(infile, line))            // 若未到文件结束一直循环
	{
		splitString(line, temp);
		vector<double> data;
		if (!size) _begin_time = stod(temp[0]);
		//cout << temp.size()<<endl;
		for (int i = 0; i < 14; ++i)data.push_back(stod(temp[i]));//共14种信息
		_GNSS_result.push_back(data);
		size++;
	}
	_end_time = _GNSS_result[size - 1][0];
	_data_size = size;
	infile.close();
	//cout << begin_time << " " << end_time;
	//cout <<" !!! "<<GNSS_result.size();
}
/*获取GNSS-RTK解算结果
epoch_index     卫星历元序号：1表示第1个
gnss_epoch      卫星结果结构体
*/

void GNSS::getGNSSres(int epoch_index, GNSSEpoch &gnss_epoch)
{
	gnss_epoch.epoch_index = epoch_index;//卫星结果历元序号
	if(epoch_index>1)gnss_epoch._sampling_interval = _GNSS_result[epoch_index - 1][1] - _GNSS_result[epoch_index - 2][1];//与上一历元的GNSS观测时的时间间隔（首个历元设1）
	else gnss_epoch._sampling_interval = 1.0;
	gnss_epoch.tow = _GNSS_result[epoch_index - 1][1];//GPS周秒
	/*位置*/
	gnss_epoch._gnss_pos(0) = _GNSS_result[epoch_index - 1][2] / 180.0*PI;//纬度(rad)
	gnss_epoch._gnss_pos(1) = _GNSS_result[epoch_index - 1][3] / 180.0*PI;//经度(rad)
	gnss_epoch._gnss_pos(2) = _GNSS_result[epoch_index - 1][4];//高程
	/*速度*/
	//注意posmind结果文件为东北天
	gnss_epoch._gnss_vel(0) = _GNSS_result[epoch_index - 1][6];//北向速度
	gnss_epoch._gnss_vel(1) = _GNSS_result[epoch_index - 1][5];//东向速度
	gnss_epoch._gnss_vel(2) = -1.0*_GNSS_result[epoch_index - 1][7];//地向速度
	/*位置方差阵*/
    //注意posmind结果文件为东北天位置噪声标准差
	double e_std = _GNSS_result[epoch_index - 1][8];
	double n_std = _GNSS_result[epoch_index - 1][9];
	double u_std = _GNSS_result[epoch_index - 1][10];
	Matrix3d ned_Dxx = Matrix3d::Zero();
	ned_Dxx(0, 0) = n_std * n_std;
	ned_Dxx(1, 1) = e_std * e_std;
	ned_Dxx(2, 2) = u_std * u_std;
	Matrix3d _DR_inv = DR_inv(gnss_epoch._gnss_pos);
	gnss_epoch._Dxx = _DR_inv * ned_Dxx*_DR_inv.transpose();
	/*速度方差阵*/
	double ve_std = _GNSS_result[epoch_index - 1][11];
	double vn_std = _GNSS_result[epoch_index - 1][12];
	double vu_std = _GNSS_result[epoch_index - 1][13];
	Matrix3d ned_Dvv = Matrix3d::Zero();
	ned_Dvv(0, 0) = vn_std * vn_std;
	ned_Dvv(1, 1) = ve_std * ve_std;
	ned_Dvv(2, 2) = vu_std * vu_std;
	gnss_epoch._Dvv = ned_Dvv;
}


