#include"imr2ascii_statement_backup.h"
#include<fstream>
#include<iostream>
#include<iomanip>

using namespace std;

int IMR::getIMR_Data(IMR_Data* idata)
{
	if (_imr_data_num > 0)
	{
		for (int i = 0; i < _imr_data_num; i++)
		{
			idata[i] = _imr_data[i];
		}
		return _imr_data_num;
	}
	else
	{
		cout << "无数据！！！" << endl;
		return -1;
	}
}
int IMR::getIMR_Data(IMR_Data* idata, int index)
{
	if (_imr_data_num > 0)
	{
		if (index > 0 && index < _imr_data_num)
		{
			*idata = _imr_data[index - 1];
			return 0;
		}
		else
		{
			cout << "历元数有误！" << endl;
			return -1;
		}
	}
	else
	{
		cout << "无数据！！！" << endl;
		return -1;
	}
}

int IMR::IMR_ReadFile()
{
	int num = 0;
	num = IMR_ReadHeader();
	if (num > 0)
	{
		_imr_data_num = num;
		_imr_data = new IMR_Data[num];
		if (num == IMR_ReadData()) return num;
		else
		{
			cout << "文件读取故障！" << endl;
			return 0;
		}
	}
	cout << "文件有误！" << endl;
	return -1;
}

int IMR::IMR_ReadHeader()
{
	fstream imr_file_in(_file_name, ios::in | ios::binary);
	if (!imr_file_in)
	{
		cout << _file_name << "文件无法打开!" << endl;
		exit(1);
		return -1;
	}
	char header[8];
	string header_str = "$IMURAW\0";
	imr_file_in.read(header, 8);
	if (header_str != header)
	{
		cout << "非imr文件！" << endl;
		return -1;
		exit(1);
	}

	int8_t bIsIntelOrMotorola = 0;
	int32_t b_delta_theta = 0;
	int32_t b_delta_accel = 0;
	double version = 0.0;
	char no_use[455];

	imr_file_in.read((char*)&bIsIntelOrMotorola, 1);
	imr_file_in.read((char*)&version, 8);
	imr_file_in.read((char*)&b_delta_theta, 4);
	imr_file_in.read((char*)&b_delta_accel , 4);
	imr_file_in.read((char*)&_data_rate, 8);
	imr_file_in.read((char*)&_gyro_scale_factor, 8);
	imr_file_in.read((char*)&_accel_scale_factor, 8);
	imr_file_in.read(no_use, 455);

	int data_count = 0;
	char imr_data_chunk[32];
	while (!imr_file_in.eof())
	{
		imr_file_in.read(imr_data_chunk, 32);
		data_count++;
	}
	imr_file_in.close();

	_is_delta_theata = b_delta_theta;
	_is_delta_velocity = b_delta_accel;

	return data_count;
}

int IMR::IMR_ReadData()
{
	fstream imr_file_in(_file_name, ios::in | ios::binary);
	if (!imr_file_in)
	{
		cout << _file_name << "文件无法打开!" << endl;
		exit(1);
		return -1;
	}

	char no_use[512];//文件头
	int data_count = 0;//
	int32_t imr_data_ag[6];
	double scale_factor[2];//缩放比例因子

	scale_factor[0] = _gyro_scale_factor;
	scale_factor[1] = _accel_scale_factor;

	//如果为增量则需要乘上数据输出频率
	if (_is_delta_theata) scale_factor[0] = scale_factor[0] * _data_rate;
	if (_is_delta_velocity) scale_factor[1] = scale_factor[1] * _data_rate;

	imr_file_in.read(no_use, 512);
	while (!imr_file_in.eof())
	{
		imr_file_in.read((char*)&_imr_data[data_count]._time, 8);
		for (int i = 0; i < 6; i++)
		{
			imr_file_in.read((char*)&imr_data_ag[i], 4);
		}
		_imr_data[data_count]._gyro_x = imr_data_ag[0] * scale_factor[0];
		_imr_data[data_count]._gyro_y = imr_data_ag[1] * scale_factor[0];
		_imr_data[data_count]._gyro_z = imr_data_ag[2] * scale_factor[0];
		_imr_data[data_count]._accel_x = imr_data_ag[3] * scale_factor[1];
		_imr_data[data_count]._accel_y = imr_data_ag[4] * scale_factor[1];
		_imr_data[data_count]._accel_z = imr_data_ag[5] * scale_factor[1];

		data_count++;
	}

	return data_count;
}

int IMR::IMR_RewriteASCII(string file_out_name)
{
	fstream file_out(file_out_name, ios::out);
	if (!file_out)
	{
		cout << "" << endl;
		return -1;
	}
	for (int i = 0; i < _imr_data_num; i++)
	{
		file_out << fixed << setprecision(6) << setw(11) << setfill(' ') << _imr_data[i]._time;
		file_out << fixed << setprecision(5) << setw(11) << setfill(' ') << _imr_data[i]._gyro_x;
		file_out << fixed << setprecision(5) << setw(11) << setfill(' ') << _imr_data[i]._gyro_y;
		file_out << fixed << setprecision(5) << setw(11) << setfill(' ') << _imr_data[i]._gyro_z;
		file_out << fixed << setprecision(5) << setw(11) << setfill(' ') << _imr_data[i]._accel_x;
		file_out << fixed << setprecision(5) << setw(11) << setfill(' ') << _imr_data[i]._accel_y;
		file_out << fixed << setprecision(5) << setw(11) << setfill(' ') << _imr_data[i]._accel_z << endl;
	}
	file_out.close();
	return 1;
}

int Imr2ASCII_TestHC()
{
	string fname;
	string outf_name;
	cout << "请输入原始文件名：" << endl;
	cin >> fname;
	IMR imr(fname);
	IMR_Data imr_data;
	
	cout << "file reading..." << endl;
	int num = imr.IMR_ReadFile();
	cout << "请输入目标文件名：" << endl;
	cin >> outf_name;
	cout << "file rewriting..." << endl;
	int flag = imr.IMR_RewriteASCII(outf_name);
	if (flag) cout << "DONE!" << endl;
	else cout << "out erorr！！！" << endl;
	return 0;
};